---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "What is Usability testing?"
description:
    - "Usability testing"
    

# Data
categories: 
    - "faq"
    - "management"
tags: management
img: "/images/projects/guvernante/1.webp"
permalink: "faq/management/Usability-testing"
---
Users are asked to complete tasks, typically while they are being observed by a researcher, to see where they encounter problems and experience confusion. If more people encounter similar problems, recommendations will be made to overcome these usability issues.

Few methods of testing:
- Usability testing — To know problems in the pre-existing solution by testing it with the users.
- Click tracking — To track user engagement, website errors, and optimization.
- A/B Testing — To test the performance of two different versions of a prototype with the users.
- Web analytics — To investigate how many users are coming on your site, where your users are coming from, how long they are staying, etc.
- System usability scale — A simple set of 10 questions to test the satisfaction level of the users
- Usability Metrics — To quantify the data you gathered from usability testing. Learn more.
- Heuristic evaluation — Set of 10 rules to ensure that the user has control over the product. Learn more.

There are many advantages of usability testing including:
- Feedback direct from the target audience to focus the project team
- Internal debates can be resolved by testing the issue to see how users react to the different options being discussed
- Issues and potential problems are highlighted before the product is launched