---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "How to develop personas?"
description:
    - "The persona is the voice of the user; typically based on user research and incorporating user goals, needs and interests."
    

# Data
categories: 
    - "faq"
    - "management"
tags: management
img: "/images/projects/guvernante/1.webp"
permalink: "faq/management/User-personas"
---
The persona is the voice of the user; typically based on user research and incorporating user goals, needs and interests.

##### Basic types of personas?
Marketing persona: For creation, we need information about the demographic of our users. The main user buying patterns are studied, along with the channels to market. All this information has a main purpose to increase sales and prioritise product features. This persona often helps describe and explain customer behaviour, but not necessarily the reasons why the actions are this way.

- Design persona: Here we are keener to look at the reactions of users, their habitats and experiences. The main purpose is using that persona in further UX research. Main focus is on goals, targets and obstacles which user have. Using in development team can help to retain focus on our goals across various members of team. Design persona needs to be short, descriptive and personalised. Using of infographics are very welcome.
- Proto persona: This persona is largely created when there is lack of funding for research. A Proto persona can be a mix of design and marketing persona. Although it is a good point to start from with building a persona, it is not reliable for long term use.

##### When is a right time to use a persona?
Every time a customer makes a request, the design changes.
Everyone on the team has a different opinion about who we are designing for (who the target user is). 

This can result in:
– Self-referential design
– The “elastic user”
– We can’t agree on which features to prioritise (what the user’s primary goals are).
– We spend time developing features that never get used (edge cases). 

##### How can we create persona from scratch?
- Collect data from all possible sources about users. Great sources are: Google Analytics, Application Database or even your own company’s marketing strategy
- Summarise data and grouping similar users to create a persona profile. For example if the site will be used by both males and females, we would need a grouping of much more than just one person, as indeed there are subtle to large variations in viewpoints and levels of appreciation men and women have for a variety of website features. A big category such as men and women can be split into sub categories, i.e. age, profession, region etc. It’s good to continue this splitting of subsets until we see a difference in personas.
- Write personas from summary data. Now you know who is your user and this is just way how to put on paper these information in most sensible way.
- Review personas with rest of team and implement all feedback into final personas.
Once a persona is created our main goal is just to make sure our persona is up to date and reflects our present users. Some persona is better than no persona, but an incorrect, ‘bad persona’ can be very misleading.

##### What describes a good persona?
We can make a simple checklist of what you must have:

- Patterns of user habits
- A focus on the present state of the user – not the future
- Personas that are realistic – not idealised
- Describes a difficult goal- not an unsolvable one
- Helps us to understand a user’s better
- Contextually
- Habitats
- Access
- Needs
- Pain points
- Goals and motivations

##### Things to be aware of:

- Poorly constructed personas undermine the credibility of all personas
- Poor communication about personas within an organization
- Lack of clarity around how personas should be used throughout design
- Lack of understanding of how design personas can be used over time
- How to maintain your persona in the long term?
- As best practice, it is common to include relevant data from each strand of research. However, this can be very time demanding, so as a minimum it is at least good to revisit personas after six months or when starting to build a new part of an application –  to ensure everything is still up to date. Once when you create a persona, then as a UX designer you must always ensure you are happy with it and maintain it.

Presently, the industry does not have much professional software that allows the use and development of personas. From one of our better projects we found xtensio.com, which can be handy to get you started.

##### Conclusion
Now that you know what you can achieve from using personas, the remaining question is to ask: when will you will be building a persona? For when you start, you will never go back.