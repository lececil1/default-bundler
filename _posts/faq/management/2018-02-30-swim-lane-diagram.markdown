---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "What is Swim lane diagram?"
description:
    - "Swimlanes add an extra level of clarity about who does what to process flowcharts. Combining the Empathy map with the Customer Journey map will create a Swim Lane diagram. This diagram serves to create a heat map of the problems that exist within each step of the customer journey."
    

# Data
categories: 
    - "faq"
    - "management"
tags: management
img: "/images/projects/guvernante/1.webp"
permalink: "faq/management/swim-lane-diagram"
---

Swimlanes add an extra level of clarity about who does what to process flowcharts. Combining the Empathy map with the Customer Journey map will create a Swim Lane diagram. This diagram serves to create a heat map of the problems that exist within each step of the customer journey.

A swimlane diagram is a type of flowchart that delineates who does what in a process.  Using the metaphor of lanes in a pool, a swimlane diagram provides clarity and accountability by placing process steps within the horizontal or vertical “swimlanes” of a particular employee, work group or department. It shows connections, communication and handoffs between these lanes, and it can serve to highlight waste, redundancy and inefficiency in a process.

**Benefits:**
- Higlight and visualize processes in company.
- Optimalization of bottlenecks, waste and other inefficiencies.
- Structurize the process to steps
- Bringing more clarity
- More integration across departments