---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "What is Time tracking?"
description:
    - "Time tracking is simply the measurement and documentation of hours worked."
    

# Data
categories: 
    - "faq"
    - "management"
tags: management
img: "/images/projects/guvernante/1.webp"
permalink: "faq/management/time-tracking"
---
Time tracking is simply the measurement and documentation of hours worked.

With the right technology, businesses can also track additional data such as efficiency and employee productivity. From project management methodologies and accounting systems to our collaboration and communication processes, time tracking can allow businesses and employees to be productive at unprecedented levels.

As my choice of weapon I am using Toggl tracker. 
