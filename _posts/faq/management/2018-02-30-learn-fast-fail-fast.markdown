---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Why to Learn fast and fail fast?"
description:
    - " The sprint helps to obtain a clear vision of the goals upfront. It forces you to make critical decisions and solve complex problems fast. This means that you and your team can save months of design, engineering and development costs. The bonus? You’ll be able to get your product to market faster because you focussed on the right thing."
    

# Data
categories: 
    - "faq"
    - "management"
tags: management
img: "/images/projects/guvernante/1.webp"
permalink: "faq/management/Learn-fast-fail-fast."
---
 The sprint helps to obtain a clear vision of the goals upfront. It forces you to make critical decisions and solve complex problems fast. This means that you and your team can save months of design, engineering and development costs. The bonus? You’ll be able to get your product to market faster because you focussed on the right thing.
