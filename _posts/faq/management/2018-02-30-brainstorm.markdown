---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "What is Brainstorming?"
description:
    - "Brainstorming is a method for generating ideas to solve a design problem. It usually involves a group, under the direction of a facilitator. The strength of brainstorming is the potential participants have in drawing associations between their ideas in a free-thinking environment, thereby broadening the solution space."
    

# Data
categories: 
    - "faq"
    - "management"
tags: management
img: "/images/projects/guvernante/1.webp"
permalink: "faq/management/Brainstorm"
---
Brainstorming is a method for generating ideas to solve a design problem. 

It usually involves a group, under the direction of a facilitator. The strength of brainstorming is the potential participants have in drawing associations between their ideas in a free-thinking environment, thereby broadening the solution space.

Brainstorming may seem to lack constraints, but success depends on the observance of eight house rules and someone acting as facilitator.

- Set a time limit – depending on the problem’s complexity, 15–60 minutes is normal.
- Begin with a target problem/brief – members should approach this sharply defined question, plan or goal and stay on topic.
- Refrain from judgment/criticism – no-one should be negative (including via body language) about any idea.
- Encourage weird and wacky ideas – further to the ban on killer phrases like “too expensive”, keep the floodgates open so everyone feels free to blurt out ideas, as long as they’re on topic.
- Aim for quantity – remember, “quantity breeds quality”; the sifting-and-sorting process comes later.
- Build on others’ ideas – it’s a process of association where members expand on others’ notions and reach new insights, allowing these ideas to trigger their own. Say “and”—rather than discourage with “but”—to get ideas closer to the problem.
- Stay visual – diagrams and Post-Its help bring ideas to life and help others see things in different ways.
- Allow one conversation at a time – keeping on track this way and showing respect for everyone’s ideas is essential for arriving at concrete results.