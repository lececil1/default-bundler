---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "How to Journey mapping?"
description:
    - "Breaking down the main task into subtask for evaluating the efficiency."
    

# Data
categories: 
    - "faq"
    - "management"
tags: management
img: "/images/projects/guvernante/1.webp"
permalink: "faq/management/Journey-map"
---

Customer journey mapping has become a favourite tool for visualising the customer’s experience. But what exactly are they, how do you create them and what is the best way to use them?

##### Identify the information you want to map
The second decision the group needs to make is what information you want to map about the user. What do you need to know at each of these key stages in their interaction? Again, this is up to you. But some common areas are:

- Tasks. What is the user trying to achieve at this stage?
- Questions. What does the user want to know at this stage?
- Touchpoints. How does the user interact with the organisation at this point?
- Emotions. What is the user feeling at this stage in the process?
- Weaknesses. How does the organisation let the user down at this stage?
- Influences. Who or what is helping to shape the user’s decision-making process at this stage?

The process of creating a customer journey map also helps identify weak points in the customer journey. Places where the organisation is letting customers down and procedures need to change.

Keeping these goals in mind is essential. It is easy to get sucked into endless discussions over different paths the user might follow. Pick a journey and tell that story. To get a holistic view for a particular user in a specific scenario so that different teams can look into and improve various parts of the journey.