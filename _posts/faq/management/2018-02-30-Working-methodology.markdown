---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Working methodology"
description:
    - " To prioritize which product features are most important to the user."
    

# Data
categories: 
    - "faq"
    - "management"
tags: management
img: "/images/projects/guvernante/1.webp"
permalink: "faq/management/research"
---


working methodology
Finally, few teams I encounter have established and communicated clear working methodologies. In other words, there is no formalised process.

These teams do have working practices. They are just not formalised and communicated to clients. This means the teams are always having to justify their approach. Often they find themselves working in ways they are unhappy with.

It is so important to formalise a teams approach. You can do this by creating a service manual or set of design principles. An approach agreed and supported by senior management. An approach that you can enforce if required. But an approach that you can also communicate to stakeholders at the start of the project.