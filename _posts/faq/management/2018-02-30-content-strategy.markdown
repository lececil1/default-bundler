---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "How to Content-strategy?"
description:
    - "Content strategy refers to the management of pretty much any tangible media that you create and own: written, visual, downloadable — you name it. Content strategy is the piece of your marketing plan that continuously demonstrates who you are and the expertise you bring to your industry."
    

# Data
categories: 
    - "faq"
    - "management"
tags: management
img: "/images/projects/guvernante/1.webp"
permalink: "faq/management/Content-strategy"
---
Content strategy refers to the management of pretty much any tangible media that you create and own: written, visual, downloadable — you name it. Content strategy is the piece of your marketing plan that continuously demonstrates who you are and the expertise you bring to your industry.

When you develop a content strategy, there are a few questions to answer.

1. Who will be reading your content?
2. What problem will you be solving for your audience(s)?
3. What makes you unique?
4. What content formats will you focus on?
5. What channels will you publish on?
6. How will you manage content creation and publication?
