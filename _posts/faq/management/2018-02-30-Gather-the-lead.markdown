---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Get the leads"
description:
    - " To prioritize which product features are most important to the user."
    

# Data
categories: 
    - "faq"
    - "management"
tags: management
img: "/images/projects/guvernante/1.webp"
permalink: "faq/management/Get-the-leads"
---

LEADS
My review will find ways to encourage more people to contact you via your website and ensure those who contact you are ready to buy.