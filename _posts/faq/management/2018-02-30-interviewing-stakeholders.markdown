---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "How to interview stakeholders?"
description:
    - "Interviewing people from across the business helps form a picture of where it is in the use of digital and customer service. These interviews also help me identify cultural and political barriers to change."
    

# Data
categories: 
    - "faq"
    - "management"
tags: management
img: "/images/projects/guvernante/1.webp"
permalink: "faq/management/interviewing-stakeholders"
---
Interviewing people from across the business helps form a picture of where it is in the use of digital and customer service. These interviews also help me identify cultural and political barriers to change.

The interviews identify opportunities for improvement, and for digital to better support the business. This information feeds into the strategic recommendations I make.

Typical question can be:
- Describe the purpose of this project in your own words.
- What’s the most important thing for us to get right?
- How would you characterize the target audience?
- If you could ask users one thing, what would it be?
- How will you know if this is successful?