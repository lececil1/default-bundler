---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "SWOT Analysis"
description:
    - " To prioritize which product features are most important to the user."
    

# Data
categories: 
    - "faq"
    - "management"
tags: management
img: "/images/projects/guvernante/1.webp"
permalink: "faq/management/swot"
---

review cover?
The scope of any review I carry out will be dependant on the type of analysis selected and the client’s business requirements. However, typically it will include areas such as:

Accessibility.
Social media engagement and reach.
User experience.
Performance.
Search engine optimisation.
Content quality.
Calls to action.
The performance of your competition.
An analysis of your analytics.
Resourcing and management.