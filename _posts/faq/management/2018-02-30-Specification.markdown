---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Focus on specifications"
description:
    - " To prioritize which product features are most important to the user."
    

# Data
categories: 
    - "faq"
    - "management"
tags: management
img: "/images/projects/guvernante/1.webp"
permalink: "faq/management/focus-on-specifications"
---


focus on specifications
Too many of the digital projects have the odds stacked against them from day one. They begin with a long specification laying out exactly what the team should be building. Specifications that are inappropriate for several reasons:

They are not based on user needs.
They are not researched for appropriateness.
They become inflexible documents. Documents that do not accommodate lessons learned in the project.
Stakeholders can read them in many ways. This can cause confusion and misunderstanding.
They are often a wish list of functionality that leads to over engineered solutions.
They are time consuming to produce. This slows down the delivery of the project.
Specifications made sense on projects where the cost of change was high. But digital allows us to adapt as new information arises. It also allows us to gather far more data than other mediums. Data that can inform our direction.

This is why prototyping and minimum viable products are so popular in digital. It allows projects to evolve, removing the need to specify everything up front.