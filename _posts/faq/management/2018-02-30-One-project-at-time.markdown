---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "One project a time"
description:
    - " To prioritize which product features are most important to the user."
    

# Data
categories: 
    - "faq"
    - "management"
tags: management
img: "/images/projects/guvernante/1.webp"
permalink: "faq/management/One-project-a-time"
---


Some teams avoid the problem of prioritising by attempting to work on all projects at the same time. To placate stakeholders they juggle projects and try to keep them all moving forward.

Every time I see this the result is the same. Progress is far slower than addressing one project at a time. This is because team members struggled to switch back-and-forth. Developers in particular find this challenging.

The best digital teams work through a stack of projects one at a time. If the teams are large they break themselves down into smaller groups. This allows them to work on more than a single project with each group owning a project. But each smaller group only handles one project at the time.