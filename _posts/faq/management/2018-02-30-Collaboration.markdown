---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Collaboration"
description:
    - " To prioritize which product features are most important to the user."
    

# Data
categories: 
    - "faq"
    - "management"
tags: management
img: "/images/projects/guvernante/1.webp"
permalink: "faq/management/collaboration"
---


A lack of collaboration
Part of the reason teams think they can run many projects is because they see projects as linear. They attempt to “manufacture” digital solutions on a “factory line”. They hand projects off between team members. The designer does her work and then hands on to the developer and so on.

But in practice, digital projects do not work like that. They need a continual back and forth between team members. They need close collaboration and cooperation.

Digital projects need a collaborative back and forth. This is why a waterfall approach is hard.

Tweet this
The result is that projects bounce back and forth, slowing everything down. For example if the developer needs the designer to make some amendments to the design, he has to wait. Wait until the designer has time free from the next project the project manager has given her.

That is why project teams work better. It allows the team to work collaboratively, moving through the project together. Yes it may lead to some team members being less busy in some parts of the project. But generally projects will progress faster.