---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Did you used Issue tracker?"
description:
    - "An issue tracking system is any software application that allows you to record and follow the progress of every customer ticket or issue in your inbox until the problem is resolved."
    

# Data
categories: 
    - "faq"
    - "management"
tags: management
img: "/images/projects/guvernante/1.webp"
permalink: "faq/management/issue-tracker"
---
Yes i was using GitLab, GitHub and Atlassian. An issue tracking system is any software application that allows you to record and follow the progress of every customer ticket or "issue" in your inbox until the problem is resolved.

The issue tracking cycle begins when the customer tells your company about their questions, and ends when the customer leaves feedback on the support they received. An effective issue tracking software ensures that this process goes smoothly, stays error-free, and makes customer service easy for everyone involved.
