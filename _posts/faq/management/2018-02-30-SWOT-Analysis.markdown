---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "SWOT Analysis"
description:
    - " To prioritize which product features are most important to the user."
    

# Data
categories: 
    - "faq"
    - "management"
tags: management
img: "/images/projects/guvernante/1.webp"
permalink: "faq/management/swot"
---

SWOT Analysis
A website SWOT analysis looks at your website in its broader context. It not only looks at the website itself but at influencing factors that are maybe holding the site back. These factors include areas such as governance, resourcing and leadership. Also, it explores untapped opportunities where the website could be providing more substantial business benefits.