---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "How to handle team communication?"
description:
    - "Team communication is the foundation of team collaboration. To work together, you must communicate. To work together well, you have to make sure your team communication is stellar."
    

# Data
categories: 
    - "faq"
    - "management"
tags: management
img: "/images/projects/guvernante/1.webp"
permalink: "faq/management/Team-communication"
---
Team communication is the foundation of team collaboration. To work together, you must communicate. To work together well, you have to make sure your team communication is stellar.

##### Communication formats and channels
The specific forms and types of communication that take place will depend on your field of work, and the structure of your team. Here’s a non-exhaustive list of team communication channels:

- Verbal team communication: team meetings, one-on-one interactions, water-cooler conversations, calls
- Written communication: paper documentation, electronic documentation, emails, team chat, tasks

##### Channels which i preffer for team communication:
- Slack - As daily real-time communication
- Email - As high priority communication with longer response time good history log
- Issue Tracker - Place to discuss development and related work

##### Importance of Team Communication
Most people who have worked in teams would agree on the importance of team communication. It seems almost commonsensical that communication affects a team’s performance, morale and work enjoyment levels.

