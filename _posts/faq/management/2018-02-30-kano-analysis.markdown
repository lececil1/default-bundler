---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "What is Kano analysis?"
description:
    - "You want to create a product roadmap with the right features. There are many different reasons why you might need to include a given feature, but what do you do in order to know which ones will make your customers happy and prefer it over others."
    

# Data
categories: 
    - "faq"
    - "management"
tags: management
img: "/images/projects/guvernante/1.webp"
permalink: "faq/management/Kano-analysis"
---
You want to create a product roadmap with the right features. There are many different reasons why you might need to include a given feature, but what do you do in order to know which ones will make your customers happy and prefer it over others. To prioritize which product features are most important to the user.

##### Strengths of Kano analysis
- One of the biggest strengths of Kano is its ability to effectively identify needs and desires of customers
- Kano provides a type of ‘route map’ for product and service development, identifying priority features for improvement and attention
- It can also be used for product introduction, as Kano helps us to understand which features should be included to increase the chances of the product or service being a success
- The analysis provided from Kano is extremely useful in terms of figuring out customers’ priorities and needs, whatever stage a product or service is at

##### How does the Kano model work?
Through the model, features and attributes of a product or service are categorised in five ways:

Threshold Attributes (Basics) (Must-have features) – these are features that customers expect the service or product to have, these aren’t features that would necessarily impress customers but can cause dissatisfaction is missing

Performance Attributes (Satisfiers) (One-Dimensional features) – these features don’t come with the deal, rather add to the enjoyment level

Excitement Attributes (Delighters) (Attractive features) – these are the crucial features that increase the product/service’s competitors edge. This is the attribute to focus on as it will put you on a pedestal among your competitors

Indifferent Attributes – these are features that customers cannot decide if they are good or bad

Reverse Attributes – these features can be high quality or performance, but not increase satisfaction levels.

In order to get to these definitions, consumers are asked two questions:

How do you feel if you have this feature? (Functional Question)

How do you feel if you do not have this feature? (Dysfunctional Question)

Both questions are answered on a five-point, single coded scale, and the below chart shows how each feature is categorised based on the answers to the functional vs dysfunctional questions.