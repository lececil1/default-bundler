---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Information architecture"
description:
    - "SVN is simply a method used to store and access a repository. ... SVN is merely a version tracker. It allows you to send file changes to an archive that tracks who changed what in each version of the files. It allows you to easily undo the changes that someone made while keeping other changes in place."
    

# Data
categories: 
    - "faq"
    - "code"
tags: code
img: "/images/projects/guvernante/1.webp"
permalink: "faq/development/Information-architecture"
---
Science of organizing the data on each page according to the user’s expectations. You can combine sitemap and information architecture into a single diagram.



