---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "What are the benefits of prototyping?"
description:
    - "A prototype, in terms of web design, is an interactive mockup of your web design. ... A website prototype is essentially a high-fidelity visual version of the site that allows you to link between screens and demonstrate how the website would work before going to build."
    

# Data
categories: 
    - "faq"
    - "design"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/design/Prototype"
---
A prototype, in terms of web design, is an interactive mockup of your web design. ... A website prototype is essentially a high-fidelity visual version of the site that allows you to link between screens and demonstrate how the website would work before going to build.

Depending on the complexity level of the website, it can be beneficial to prototype at an early wireframe stage, even if this is just for the designers benefit. In some cases, working with flat visuals can blind a designer to some areas of navigation and functionality that could benefit the design. Working with wireframe prototypes can help identify areas of improvement at a very early stage of design, thus saving time later on when working with high-fidelity visuals.

##### What are the benefits of prototyping?

Prototyping is hugely beneficial in the design process as it allows us to save a lot of time early on in a web project. Identifying areas for improvement during the design stage is a lot easier to rectify than finding out these problems once the site has gone into development. Prototyping is also beneficial to show users who may not understand flat visuals as easily as an interactive version of the site.

##### There are few types of prototypes
- Paper prototype - This is usefull step to save long hours infront of computer and bring rapid prototyping.
- Low fidelity prototype — For immediate testing with the user using a minimum of resources and consist of wireframes. Some software for app design: UX, Balsamiq, inVision.
- High fidelity prototype — A look-alike of the final product to have a realistic feel while testing. Some software for app design: Figma, Adobe XD, Webflow, Sketch, UX Pin, Invision studio. 
- Interactive prototype — High fidelity prototype + most of the animations that will happen when the user interacts with the app. Some software: Protopie, Principle.