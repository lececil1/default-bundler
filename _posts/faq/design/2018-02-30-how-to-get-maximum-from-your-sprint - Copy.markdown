---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "How to get maximum from Design Sprint?"
description:
    - "The tips gained from practical experience that I have outlined in the article will only make this effort more successful. So as you plan your next design sprint, remember how you can maximize your experience."
    

# Data
categories: 
    - "faq"
    - "design"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/design/design-sprint"
---
Design Sprint is a unique five day process for validating ideas and solving big challenges through prototyping and testing ideas with customers. The sprint is by far the most effective process I’ve seen to drive customer-first decision making in a design thinky way. 

How you can maximize your experience:

1. Pre-sprint

- Schedule early
- Research your customers

2. During-sprint

- Start with the customer
- Prototype at the right level
- Conduct value proposition interview

3. After-sprint

- Put someone in charge
- Iterate and validate
- Get to work

Now it’s time to test out your new superpower!
