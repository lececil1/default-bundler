---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "What is Storyboard?"
description:
    - "A storyboard is a graphic organizer that plans a narrative. Storyboards are a powerful way to visually present information; the linear direction of the cells is perfect for storytelling, explaining a process, and showing the passage of time."
    

# Data
categories: 
    - "faq"
    - "design"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/management/Storyboard"
---
A storyboard is a graphic organizer that plans a narrative. Storyboards are a powerful way to visually present information; the linear direction of the cells is perfect for storytelling, explaining a process, and showing the passage of time.

We all need to plan for something, whether it be at work, school, or home. Storyboarding out your desired outcome, even in a very simplified manner, helps you prepare for potential issues, make sure your plan is sound, and/or communicate ideas with others. There are many uses for storyboards in the entertainment industry, the business world, and education. 