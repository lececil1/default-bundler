---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "What is Four-step sketch method"
description:
    - "The four-step sketch method forces you to create solutions in an effective manner whilst iterating on each variation along the way."
    

# Data
categories: 
    - "faq"
    - "design"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/design/four-step-sketch-method"
---
The four-step sketch method forces you to create solutions in an effective manner whilst iterating on each variation along the way.

The four-step sketch is an exercise that helps people to create well-formed concepts through a structured process that includes:
- Review key information
- Start design work on paper, 
- Consider multiple variations,
- Create a detailed solution.

This exercise is preceded by a set of other activities allowing the group to clarify the challenge they want to solve. Four Step Sketch exercise fits into a Design Sprint

##### Brief outline:
- Notes. Start with twenty minutes to take notes of the goal, opportunities and inspiration you’ve collected earlier on.
- Ideas. Spend another twenty minutes drawing out rough ideas to form your thoughts.
- Crazy 8s. Take your strongest solution and sketch out eight different variations of it in eight minutes, known as the ‘Crazy 8s’ exercise.
- Solution sketch. Draw a detailed end to end solution for the problem in the next thirty minutes or more.