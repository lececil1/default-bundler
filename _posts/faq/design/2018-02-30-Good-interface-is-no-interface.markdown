---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "No interface is good interface?"
description:
    - "Golden Krishna postulates that people don’t want more screens, they want fewer. It makes little sense as to why there is such celebration regarding screen-based solutions. He goes on to make the point that designers should be trying to make our lives as easy as possible and that doesn’t necessarily require screen-based thinking."
    

# Data
categories: 
    - "faq"
    - "design"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/design/No-interface"
---
Golden Krishna postulates that people don’t want more screens, they want fewer. It makes little sense as to why there is such celebration regarding screen-based solutions. He goes on to make the point that designers should be trying to make our lives as easy as possible and that doesn’t necessarily require screen-based thinking.

This should be a sigh of relief for designers. Instead of designing for multiple, disparate groups and screens, there can be a focus on designing around the behavior that humans already have, such as speech and movement.

Humans are already experts in this so creating interfaces (or having no interface) which can use these behaviors will already change the user experience entirely. Whether those experiences are good or bad remains to be seen.