---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "What is Style guide?"
description:
    - "A style guide is a document that provides guidelines for the way your brand should be presented from both a graphic and language perspective. The purpose of a style guide is to make sure that multiple contributors create in a clear and cohesive way that reflects the corporate style and ensures brand consistency with everything from design to writing"
    

# Data
categories: 
    - "faq"
    - "design"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/design/Style-guide"
---
A style guide is a document that provides guidelines for the way your brand should be presented from both a graphic and language perspective. The purpose of a style guide is to make sure that multiple contributors create in a clear and cohesive way that reflects the corporate style and ensures brand consistency with everything from design to writing


There are few types:
- Visual style guide — For maintaining consistency in the visual design. 
- Editorial Style Guide - Rules for company voice
- UI Kit - Visual guide for maintaining components, system and communication with developers 

Some tools that I frequently use: KSS, XD, CSS, Fontastic, Flaticon, Google fonts, Adobe creative suite.
