---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "What is The Golder Ratio?"
description:
    - "The golden ratio is often confused with the rule of thirds, but make no mistake they are different. The golden ratio looks at what proportions are naturally most visually appealing."
    

# Data
categories: 
    - "faq"
    - "design"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/design/The-Golden-Ratio"
---
The golden ratio is often confused with the rule of thirds, but make no mistake they are different. The golden ratio looks at what proportions are naturally most visually appealing. 

This ratio has been used in design, architecture and engineering for hundreds of years. It even has been tied to what features we find most attractive in people (both facial features and body types). The golden ratio can be described as a ratio with in the elements of a form, such as height to width, approximating 0.618.

When applied to rectangles you can continue to create smaller dissections of the shape using the .618 ratio, which creates a natural spiral pattern. This can be seen in nature by examining sea shells. This ratio has been used through out history, in everything from the craftsmanship of violins to the design of the Parthenon and Stonehendge.

It is unlikely that some of these items were created with the golden ratio in mind, rather the creators likely preferred the visual appeal of the design when using these ratios.

Ultimately the golden ratio is more likely to produce visually pleasing compositions.

