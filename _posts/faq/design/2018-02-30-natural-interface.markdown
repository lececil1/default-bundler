---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "What means Natural interface?"
description:
    - "Whether it is a signup flow, a multi-view stepper, or a monotonous data entry interface, forms are one of the most important components of digital product design. This article focuses on the common dos and don’ts of form design. Keep in mind that these are general guideline and there are exceptions to every rule."
    

# Data
categories: 
    - "faq"
    - "design"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/design/natural-interface"
---
A natural interface is a type of user interface that is designed to feel as natural as possible to the user. The goal of an NUI is to create seamless interaction between the human and machine, making the interface itself seem to disappear. Feels natural to use because it fits the skills and context of the user. 

Four guidelines for designing natural interface.

- An NUI should take advantage of the users’ existing skills and knowledge.
- An NUI should have a clear learning path and allow both novice and expert users to interact in a natural way.
- Interaction with an NUI should be direct and fit the user’s context.
- Whenever possible, you should prioritize taking advantage of the user’s basic skills.

