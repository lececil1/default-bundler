---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Can you run more sprints?"
description:
    - "The design team would ideally conduct a few sprints ahead of the development team’s sprints. For example, if the development team is working in two-week sprints, then designers should schedule their sprints accordingly every two weeks, regardless of what type of design sprint they use. At the very least, the design sprints should consist of iterating on the prototype, testing with users, and passing off to the developers, ensuring that only customer-validated features are developed."
    

# Data
categories: 
    - "faq"
    - "design"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/design/run-more-sprints"
---

The design team would ideally conduct a few sprints ahead of the development team’s sprints. For example, if the development team is working in two-week sprints, then designers should schedule their sprints accordingly every two weeks, regardless of what type of design sprint they use. 

At the very least, the design sprints should consist of iterating on the prototype, testing with users, and passing off to the developers, ensuring that only customer-validated features are developed.
