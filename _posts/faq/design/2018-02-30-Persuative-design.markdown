---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Should i use Persuative design?"
description:
    - "As humans, we hate to make decisions. Our minds spend enormous amounts of energy, on building shortcuts in our memory. Shortcuts that help us not reevaluate the same thing twice. Making a rational decision, based on real evidence, consumes a lot of energy in our brains."
    

# Data
categories: 
    - "faq"
    - "design"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/design/Persuative-design"
---
As humans, we hate to make decisions. Our minds spend enormous amounts of energy, on building shortcuts in our memory. Shortcuts that help us not reevaluate the same thing twice. Making a rational decision, based on real evidence, consumes a lot of energy in our brains...

As smart as our brains are, they constantly try to find ways to avoid spending energy – or doing real thinking. Instead, it creates shortcuts, that allow us to make quick decisions, that are true, most of the time. These shortcuts are called cognitive biases. 

If we know about the cognitive biases, the shortcuts our minds have created for us, we can utilize these, to aid our users in making decisions faster.

Persuasive design seeks to document and utilize our cognitive biases and similar insights from psychology into persuasive patterns so that they can be more easily applied to product design. By documenting recurring solutions, based on psychology, that have solved common design problems, persuasive patterns are standard reference points for the experienced product designer.

To give you a more concrete understanding of what Persuasive Design, I’ll show you 6 particularly powerful applications persuasive patterns:

1. Getting more valuable data from users by using Recognition over Recall.
2. Providing a feeling of closure by rewarding users Completion of a goal.
3. Establishing credibility by playing on Authority.
4. Making decision processes easier on users by utilizing their Status-Quo Bias.
5. Accommodating for the experience over time by providing Appropriate Challenges.
6. Directing user attention by closing off detours by Tunneling your users.
