---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "What is Design framework"
description:
    - "You’ve probably heard these terms: design framework, UI framework, UI kit, or pattern library. They all refer to the same thing—a system of design standards, templates, UI patterns, and components that are used throughout a product and serve its design language."
    

# Data
categories: 
    - "faq"
    - "design"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/design/Design-framework"
---
You’ve probably heard these terms: design framework, UI framework, UI kit, or pattern library. They all refer to the same thing—a system of design standards, templates, UI patterns, and components that are used throughout a product and serve its design language.

##### What Is a Design Framework?
Every user interface design starts with an empty artboard, and every designer has a process they use to transform that blank canvas into a fully functioning product. This process includes all the small design components created, and the steps the designer goes through to build a cohesive whole, from colors to buttons and everything in between.

Often, this work is repetitive and can be consolidated and made more efficient by creating a system of interrelated patterns and components. In other words, a design framework.

##### Design frameworks solve several problems, including:
- Eliminating inconsistencies in the product design.
- Increasing collaboration, efficiency, and communication between design and product teams.
- Making design updates later in the design process less frustrating.