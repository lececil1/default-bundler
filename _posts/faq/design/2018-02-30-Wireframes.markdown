---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "What are Wireframes?"
description:
    - "Wireframing is a way to design a website service at the structural level. A wireframe is commonly used to lay out content and functionality on a page which takes into account user needs and user journeys. Wireframes are used early in the development process to establish the basic structure of a page before visual design and content is added."
    

# Data
categories: 
    - "faq"
    - "design"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/design/Wireframes"
---
Wireframing is a way to design a website service at the structural level. A wireframe is commonly used to lay out content and functionality on a page which takes into account user needs and user journeys. Wireframes are used early in the development process to establish the basic structure of a page before visual design and content is added.

##### A wireframe is much easier to adapt than a concept design
It is quicker and cheaper to review and amend the structure of the key pages in a wireframe format. Iterating the development of the wireframes to a final version will provide the client and the design team confidence that the page is catering to user needs whilst fulfilling the key business and project objectives.

##### Advantages of Wireframing
One of the great advantages of wireframing is that it provides an early visual that can be used to review with the client. Users can also review it as an early feedback mechanism for prototype usability tests.

##### Disadvantages of Wireframing
As the wireframes do not include any design, or account for technical implications, it is not always easy for the client to grasp the concept. 