---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "What is Design Strategy?"
description:
    - "Design strategy is the term used to describe the nexus between corporate strategy and design thinking. Corporate strategy is the traditional method that businesses and other similar entities use to identify, plan, and achieve their long term objectives and goals."
    

# Data
categories: 
    - "faq"
    - "design"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/design/Digital-Strategy"
---
Design strategy is the term used to describe the nexus between corporate strategy and design thinking. Corporate strategy is the traditional method that businesses and other similar entities use to identify, plan, and achieve their long term objectives and goals.

Design Thinking is a methodology that provides a solution-based approach to solving problems by engaging the end-users.

Any design strategy should address the following:

- Where are we now?
- Where do we want to be?
- How will we get there?
- Existing problems and ongoing challenges
- Current benefits and successes to be leveraged
- Unmet client/customer needs
- Changing client/customer behaviors and attitudes
- Emerging ideas and trends
- Opportunities to differentiate

Steps:
- REVIEW – What are the design’s measurable goals and objectives?
- SCAN – What internal and external factors impact the design?
- FORM – How will the design work to meet these challenges and opportunities?
- IMPLEMENT – What exactly will we do? Who exactly will do it? How will it be done?
- EVALUATE – How well does the design achieve its goals and objectives?
- MAINTAIN – Do we need adjustments or do we keep the design as it is?

Here is a useful framework for implementing strategic thinking in design:

- Assess - gain an understanding of the project in a holistic way, including an analysis of constraints, opportunities, and seeing the bigger picture through the lens of the business.
- Understand - make sense of the project in terms of the design and business goals. Tie the project’s outcome back to business objectives and show key results that will help support those objectives.
- Learn - formulate the elements of the strategy by planning tasks and milestones that directly support the core objectives. Ask a lot of strategic questions. For example: “Who are we designing this product for?”
- Execute - act on the strategy and make sure the entire team is included. Strategic thinking works best in collaboration.
- Check - as tasks are accomplished during the design process, it’s a good idea to reassess their effectiveness in achieving the goals and outcomes intended.