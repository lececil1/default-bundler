---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Why to create Empathy map?"
description:
    - "The Empathy map is a visual way to better understand your users and prioritise their needs. The map helps to identify any key themes and problems affecting your users based on their quotes, actions, behaviours, pains and feelings captured throughout the user research and expert interviews."
    

# Data
categories: 
    - "faq"
    - "design"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/design/empathy-mapping"
---

The Empathy map is a visual way to better understand your users and prioritise their needs. The map helps to identify any key themes and problems affecting your users based on their quotes, actions, behaviours, pains and feelings captured throughout the user research and expert interviews.

Empathy maps are most useful at the beginning of the design process after user research but before requirements and concepting. The mapping process can help synthesize research observations and reveal deeper insights about a user’s needs. (The maps are most effective when based on research data, but like provisional personas, can be built using knowledge from internal participants or using existing personas.) It can help guide the construction of personas or serve as a bridge between personas and concept deliverables.

##### The benefits include:

- Better understanding of the user
- Distilled information in one visual reference
- Callouts of key insights from research
- Fast and inexpensive
- Easily customizable based on available information and goals
- Common understanding among teams

In order to create map we need to empathise with our user. And those are questions we should be asking ourself.

- Who is the persona for the map?
This is the user who you want to understand and empathize with. Summarize his or her situation and role. If you have multiple personas, each one will need their own map.

- What is the desired outcome?
- What does the person see?
- What does she DO and SAY?
- What does she HEAR?
- What does she THINK and FEEL?

##### Don’t get hung up on what goes where.
Some participants might be concerned about putting things in the “right” quadrant. (e.g. Is that a pain or a feeling? Did she see it or hear it?) If you have multiple groups working on building out maps for the same user. there will be nuances on how people categorize things. That’s okay. The goal isn’t to correctly classify information, it’s to identify with the user.