---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "How to Create an Effective Customer Journey Map?"
description:
    - "The Customer Journey map helps to visualise a customer’s end to end experience with your product or service. This allows the team to narrow down a broad challenge to a specific target for the sprint."
    

# Data
categories: 
    - "faq"
    - "design"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/design/customer-journey"
---

The Customer Journey map helps to visualise a customer’s end to end experience with your product or service. This allows the team to narrow down a broad challenge to a specific target for the sprint.

##### Where to start?
This isn't something you can assume or predict based on your internal perspective. A customer journey is very specific to the physical experiences your customers have. Thus, the best way to understand the journeys of your customers is by asking them.

##### Visualize and map their experience.
Most customer journey maps start as excel sheets that outline key events, customer motivations, and areas of friction within the user’s experience. Then, this information is combined into a comprehensive visual that describes an average experience with your business.

##### Benefits of mapping
However, breaking down the customer journey phase by phase, aligning each step with a goal, and restructuring your touchpoints accordingly are essential steps towards maximizing customer success.

##### Refocuse company perspective
By mapping out the customer journey, you can understand what is interesting and helpful to your customers about your company and website, and what is turning them away. You can accordingly create the kind of content that will attract them to your company and keep them there.

##### New target customer base
Researching the needs and pain points of your typical customers and mapping out their journey will give you a good picture of the kinds of people who are trying to achieve a goal with your company. Thus, you can hone in your marketing to that specific audience.

#### Creating customer Journey map
Steps:
1. Set clear objectives for the map.
2. Profile your personas and define their goals.
3. Highlight your target customer personas. 
4. List out all the touchpoints.
5. Identify the elements you want your map to show.
6. Determine the resources you have and the ones you'll need.  
7. Take the customer journey yourself.
8. Make necessary changes.

Some examples of good questions to ask are:
- How did you hear about our company?
- What first attracted you to our website?
- What are the goals you want to achieve with our company? In other words, what problems are you trying to solve?
- How long have you / do you typically spend on our website?
- Have you ever made a purchase with us? If so, what was your deciding factor?
- Have you ever interacted with our website with the intent of making a purchase but decided not to? If so, what led you to this decision?
- On a scale of 1 to 10, how easy is it for you to navigate our website?
- Did you ever require customer support? If so, how helpful was it, on a scale of 1 to 10?
- Is there any way that we can further support you to make your process easier?