---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "How to use colors?"
description:
    - "Color is the most important element in a UI design framework hierarchy—every single component in a design uses color. Color elicits strong reactions and emotions in people and sets the overall look, feel, and tone of a product."
    

# Data
categories: 
    - "faq"
    - "design"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/design/color"
---
Color is the most important element in a UI design framework hierarchy—every single component in a design uses color. Color elicits strong reactions and emotions in people and sets the overall look, feel, and tone of a product.

##### A good practice is to divide colors into groups:
- Primary colors are the main brand colors, typically used to create the general color scheme of a project and for crucial components like buttons.
- Secondary colors complement the primary palette—often they’re different shades, saturations, or tints of the primary colors. Grayscale is commonly used for typography or backgrounds. Somewhere between five to eight levels of gray should be enough.
- System feedback colors are an important group that designers often forget about. These colors display system messages, including alerts, warnings, and notifications.

##### Using Color Harmony
Harmony can be defined as a pleasing arrangement of parts, whether it be music, poetry, color. There are many theories for harmony. The following illustrations and descriptions present some basic formulas.

1. A color scheme based on analogous colors
Analogous colors are any three colors which are side by side on a 12-part color wheel, such as yellow-green, yellow, and yellow-orange. Usually one of the three colors predominates.

2. A color scheme based on complementary colors
Complementary colors are any two colors which are directly opposite each other, such as red and green and red-purple and yellow-green. These opposing colors create maximum contrast and maximum stability.

3. A color scheme based on nature
Nature provides a perfect departure point for color harmony. In the illustration above, red yellow and green create a harmonious design, regardless of whether this combination fits into a technical formula for color harmony.

