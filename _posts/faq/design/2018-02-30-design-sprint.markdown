---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "How to use Design sprints?"
description:
    - "Design Sprint is a unique five day process for validating ideas and solving big challenges through prototyping and testing ideas with customers. The sprint is by far the most effective process I’ve seen to drive customer-first decision making in a design thinky way."
    

# Data
categories: 
    - "faq"
    - "design"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/design/design-sprint"
---
Design Sprint is a unique five day process for validating ideas and solving big challenges through prototyping and testing ideas with customers. The sprint is by far the most effective process I’ve seen to drive customer-first decision making in a design thinky way. 

##### What Is A Sprint?

- Development sprint: a set period of time for software development work and review.
- Design sprint: set period of time for the design team to create functional designs ahead of the development sprint.
- Google design sprints: a 5-day process to understand if an idea maps to a customer need or opportunity without having to launch a minimal product.


##### In five days, the Design Sprint will help you to:
- Understand. Map out the problem and pick an important area to focus.
- Ideate. Sketch out competing solutions on paper.
- Decide. Make decisions and turn your ideas into a testable hypothesis.
- **Prototype.** Hack together a realistic prototype.
- **Test.** Get feedback from real live users.
