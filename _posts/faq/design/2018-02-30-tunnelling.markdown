---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "How to use Funnels?"
description:
    - "Guiding users through a process or experience provides opportunities to persuade along the way. Set up a funnel to provide opportunities to expose users to information and activities and ultimately to persuasion."
    

# Data
categories: 
    - "faq"
    - "design"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/design/funnels"
---
Guiding users through a process or experience provides opportunities to persuade along the way. Set up a funnel to provide opportunities to expose users to information and activities and ultimately to persuasion.

A conversion funnel, also known as a sales or marketing funnel, describes the process you create to attract potential buyers to your website and guide them to take some desired action. The actions can range from signing up for your email list to buying your products and services.

Lead users through a predetermined sequence of actions or events, step by step. When users enter a tunnel, they give up a certain level of self-determination – once they have entered the tunnel, they have committed to experiencing every twist and turn along the way.

**Tips:**
- Don't take away the user’s sense of control. 
- Removing all unnecessary functionality that can possibly distract their attention from completing the process.


