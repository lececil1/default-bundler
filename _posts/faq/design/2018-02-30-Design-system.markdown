---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "What are Design Systems?"
description:
    - "Once the site reaches a certain size, control becomes harder. Different content contributors add to the site with little coordination. Rolling out new designs becomes hard on such an extensive website and so is done piecemeal over time. The result is a fractured user experience."
    

# Data
categories: 
    - "faq"
    - "design"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/design/Design-System"
---
Once the site reaches a certain size, control becomes harder. Different content contributors add to the site with little coordination. Rolling out new designs becomes hard on such an extensive website and so is done piecemeal over time. The result is a fractured user experience.

The creation of service manuals and design principles usually start with workshopping exercises to gain consensus on a future approach. Following that, a draft can be produced and sold to the rest of the organisation.

A design system consists of four elements that help manage larger websites. These are:
- **A Set of Components.** These are sometimes known as a pattern library, reusable UI elements and associated code that brings consistency to your website.
- **Design Principles.** A set of guidelines that define how your organisation approaches designing online experiences. They are a framework for decision making.
- **Content Style Guide.** A set of guidelines for content creators which help ensure consistency in the tone of voice across your website.
- **A Service Manual.** Documentation that covers digital governance and how you manage your digital projects.

A design system isn’t just about the user interface, it is also about the copy.