---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Gestalt Principles"
description:
    - "Important set of principles. To made your interface more simple and experience better."

# Data
categories: 
    - "faq"
    - "psychology"
tags: psychology
img: "/images/projects/guvernante/1.webp"
permalink: "faq/psychology/Gestalt-Principles"
---
Important set of principles. To made your interface more simple and experience better...

Principles include:

- Closure (Reification): Preferring complete shapes, we automatically fill in gaps between elements to perceive a complete image; so, we see the whole first.
- Common Fate: We group elements that move in the same direction.
- Common Region: We group elements that are in the same closed region.
- Continuation: We follow and “flow with” lines.
- Convexity: We perceive convex shapes ahead of concave ones.
- Element Connectedness: We group elements linked by other elements.
- Figure/Ground (Multi-stability): Disliking uncertainty, we look for solid, stable items. Unless an image is truly ambiguous, its foreground catches the eye first.
- Good Form: We differentiate elements that are similar in color, form, pattern, etc. from others—even when they overlap—and cluster them together.
- Meaningfulness (Familiarity): We group elements if they form a meaningful or personally relevant image.
- Prägnanz: We perceive complex or ambiguous images as simple ones.
- Proximity (Emergence): We group closer-together elements, separating them from those farther apart.
- Regularity: Sorting items, we tend to group some into larger shapes, and connect any elements that form a pattern.
- Similarity (Invariance): We seek differences and similarities in an image and link similar elements.
- Symmetry: We seek balance and order in designs, struggling to do so if they aren’t readily apparent.
- Synchrony: We group static visual elements that appear at the same time.
