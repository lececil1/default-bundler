---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Cognitive load"
description:
    - "If users find interaction with an interface difficult, their mental effort or cognitive load is high. Users don't want keep thinking about how to manipulate the interface..."
    

# Data
categories: 
    - "faq"
    - "psychology"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/psychology/Cognitive-load"
---

If users find interaction with an interface difficult, their mental effort or cognitive load is high. Users don't want keep thinking about how to manipulate the interface...

Instead they want to focus on achieving a task – hence we want to keep the cognitive load to a minimum. 

To achive that we use user knowledge. User primarily applies basic knowledge and simple skills during the interaction. This means interface should be easy to use and learn. A good example of basic knowledge is our understanding of objects in the physical world. You should prioritize teaching the users simple skills rather than having them use their existing complex skills. In this sense, the Cognitive load guideline is in opposition to the Instant expertise guidelines; nevertheless, basic skills require less effort in the long run and allow you to target a broader user group.