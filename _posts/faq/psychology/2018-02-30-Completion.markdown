---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Completion"
description:
    - "If your application is geared toward a purpose with an end goal, you can utilize the fact that our need for closure drive us toward a well defined end-goal."
    

# Data
categories: 
    - "faq"
    - "psychology"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/psychology/completion"
---
If your application is geared toward a purpose with an end goal, you can utilize the fact that our need for closure drive us toward a well defined end-goal...
 

##### Setup interesting challenges
Our need for closure and completion drives us toward action. Find ways to celebrate completion in order to motivate and engage users to take action. Divide larger tasks into sub-tasks, and reward users for completion of each. 

##### Devil is in details
Use the opportunity to set expectations and to communicate the overall progress and what is next. Provide a feeling of closure by rewarding users completion of a goal.


