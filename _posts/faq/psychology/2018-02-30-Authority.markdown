---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Authority"
description:
    - "People, who identify with authority figures, trust their taste and often believe that it fits their own – or at least they wish it did."
    

# Data
categories: 
    - "faq"
    - "psychology"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/psychology/authority"
---

People, who identify with authority figures, trust their taste and often believe that it fits their own – or at least they wish it did...

##### Find the authorities
If you have experts on your team, or if the people you work with are in some way authorities, then be sure to show them off to lend credibility to the product you sell.

##### Power of authority
We have a sense of duty to authority that makes us unable to defy their wishes. Authority help define the role we take upon ourselves and the roles we put on others. We are often as vulnerable to the symbols of authority as to the substance

##### Your brand 
For user your brand is authority. Investing in branding can help to increase a trust and estabilishing authority. This give you great power but also great responsibility. 
