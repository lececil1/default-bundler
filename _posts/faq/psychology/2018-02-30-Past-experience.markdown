---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Past experiences"
description:
    - " Past experience is perhaps the weakest gestalt principle. In conjunction with any of the other principles, the other principle will dominate over the past experience principle.."
    

# Data
categories: 
    - "faq"
    - "psychology"
tags: psychology
img: "/images/projects/guvernante/1.webp"
permalink: "faq/psychology/Past-experiences"
---

“Elements tend to be perceived according to an observer's past experience.” 

Past experience is perhaps the weakest gestalt principle. In conjunction with any of the other principles, the other principle will dominate over the past experience principle.

Past experiences are unique to the individual, so it’s difficult to make assumptions about how it will be perceived. However, there are common experiences we all share. For example, a meaning of color arises out of our past experience.s





