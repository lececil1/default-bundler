---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Jakob’s law"
description:
    - "This law states that your users prefer your website to work in the same way as other websites. For instance, if your website has lots of content, there should be a search function, your website footer should contain links to important pages, your website logo should be clickable and linked to the homepage."
    

# Data
categories: 
    - "faq"
    - "psychology"
tags: psychology
img: "/images/projects/guvernante/1.webp"
permalink: "faq/psychology/Jakobs-law"
---

Jakob’s law was invented by Jakob Neilsen, a user advocate. This law states that your users prefer your website to work in the same way as other websites... 

For instance, if your website has lots of content, there should be a search function, your website footer should contain links to important pages, your website logo should be clickable and linked to the homepage. Users do not like surprises, they prefer something that is familiar so they wouldn’t need to learn how to use your website.

