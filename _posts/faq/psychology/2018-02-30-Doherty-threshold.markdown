---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Doherty Threshold"
description:
    - "Productivity soars when a computer and its users interact at a pace (<400ms) that ensures that neither has to wait on the other."
    

# Data
categories: 
    - "faq"
    - "psychology"
tags: psychology
img: "/images/projects/guvernante/1.webp"
permalink: "faq/psychology/Doherty-Treshold"
---
Productivity soars when a computer and its users interact at a pace (<400ms) that ensures that neither has to wait on the other.

##### KEY TAKEAWAYS
- Provide system feedback within 400ms in order to keep users’ attention and increase productivity.
- Use perceived performance to increase response time and reduce the perception of waiting.