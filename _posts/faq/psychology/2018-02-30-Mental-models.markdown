---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Mental Models"
description:
    - "The Mental Model law states that it is significantly easier for users to understand and learn something new if they can model it off of something they already understand."
    

# Data
categories: 
    - "faq"
    - "psychology"
tags: psychology
img: "/images/projects/guvernante/1.webp"
permalink: "faq/psychology/Mental-Models"
---
The law states that it is significantly easier for users to understand and learn something new if they can model it off of something they already understand...

This is why the concept of tabs works so well and why operating systems are modeled off of real world office situations (folders, files, desktop, etc…)

We can use this concept in making our designs easier to use as well as more effective visually. There are times where it would be effective to model our designs off of real world situations or objects. Consider designs that mimic desktops, papers or offices. Users can learn, understand and draw meaning from these types of designs because they can relate it to their understanding of the objects in real life.

