---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Progressive Learning"
description:
    - "We should allow novice users to learn progressively, how to use the user interface. Here, you need to lay out a clear learning path for users, one that allows them to start with basic skills and move on to something more advanced step by step, in increments. Never overwhelm novice users with too many options."
    

# Data
categories: 
    - "faq"
    - "psychology"
tags: psychology
img: "/images/projects/guvernante/1.webp"
permalink: "faq/design/Progressive-learning"
---
We should allow novice users to learn progressively, how to use the user interface. Here, you need to lay out a clear learning path for users, one that allows them to start with basic skills and move on to something more advanced step by step, in increments. 

##### Limit number of options
Never overwhelm novice users with too many options. Instead, keep it simple—think ‘baby steps’. This way of learning imitates the way we learn many physical skills. 

##### Dont stand in way of experts
Progressive learning path is important to novices, but standing in the way of expert users is every bit as vital. Veteran users need—and should be allowed—to use the skills they already have. 

##### Break the learning curve 
If expert users are forced to go through a long learning path, they will become frustrated. You accomplish this compromise by breaking complex tasks into a subset of basic tasks. If you cannot avoid more complex tasks, you should keep them to a strict limit as best you can. Likewise, make sure they don’t feature in the basic interface that the novice user encounters. 

Building up these basic tasks for the users of your designs is rewarding. Always apply this principle to any interface you haveS.