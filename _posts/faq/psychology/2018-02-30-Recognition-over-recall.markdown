---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Recognition over Recall"
description:
    - " Asking users to name 3 things from memory consumes remarkably more brain energy than if we ask users to select 3 things from a predefined list. Every time we make users think, there is a considerable chance we will lose them and they will drop out..."
    

# Data
categories: 
    - "faq"
    - "psychology"
tags: psychology
img: "/images/projects/guvernante/1.webp"
permalink: "faq/psychology/Recognition-over-Recall"
---

Asking users to name 3 things from memory consumes remarkably more brain energy than if we ask users to select 3 things from a predefined list. Every time we make users think, there is a considerable chance we will lose them and they will drop out...

##### Previous experience
It’s easier to recognize things we have previously experience than it is to recall them from memory. Recognition tasks provide memory cues that facilitate searching through memory. This is why a familiar option is often selected over an unfamiliar option – even when the unfamiliar option may be the best choice. 

##### Dont ask user to think 
Favoring recognition over recall help build a smooth and easy ease process that help users spend less energy. Instead of asking users to list things from memory, try complementing or replacing empty form fields with defined, random, and intelligent choices to choose or rate. Use visual imagery, auto-complete, and multiple-choice options.



