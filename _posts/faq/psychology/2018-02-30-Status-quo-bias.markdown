---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Status Quo Bias"
description:
    - "As humans, we rely on our emotions and experiences from our past, in order to make decisions faster. We imprint past decisions, which was deemed successful, as shortcuts in our brains..."
    

# Data
categories: 
    - "faq"
    - "psychology"
tags: psychology
img: "/images/projects/guvernante/1.webp"
permalink: "faq/psychology/Status-Quo-Bias"
---

As humans, we rely on our emotions and experiences from our past, in order to make decisions faster. We imprint past decisions, which was deemed successful, as shortcuts in our brains...

##### Our decisions are biased by our experience
These shortcuts allow us to react much more quickly, when facing a similar decision in the future, than if we were to rationally evaluate all decisions all the time. We rely on these heuristic shortcuts, when making decisions every day, and tend to avoid evaluating decisions rationally, when we can.
 
##### Complex decision is hardest
The more complex a decision gets, the harder it is for us, to rely on heuristic shortcuts made from past decisions, to approximate rational thinking. Instead, we tend to accept default options, instead of comparing the actual benefit to the actual cost. We tend to rely either on our own past experiences or the experiences of others.

##### Most popular
Simply stating what options are more popular or preselecting default choices, is often enough to influence a decision, that we also tend to stick with as it allows users to avoid thinking too hard about our choices.




