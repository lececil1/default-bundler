---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Weber’s Law"
description:
    - "This law states that your users prefer your website to work in the same way as other websites. For instance, if your website has lots of content, there should be a search function, your website footer should contain links to important pages, your website logo should be clickable and linked to the homepage."
    

# Data
categories: 
    - "faq"
    - "psychology"
tags: psychology
img: "/images/projects/guvernante/1.webp"
permalink: "faq/psychology/Webers-Law"
---
When we redesign a product, we should think about how the users adapt to the changes. Usually, if your product has a drastic change, no matter how good the new design is, the users would still think the old one is better. This is a natural human behaviour. 

##### Just Noticeable Difference
What you should do instead is — change gradually, so gradually that the users could not see a significant difference. This helps them adapt to and accept the new design.