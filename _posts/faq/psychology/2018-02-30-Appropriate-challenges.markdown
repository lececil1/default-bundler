---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Appropriate challenges"
description:
    - "If we give new users a task that is too hard, they are going to feel stress and anxiety. If we give users a challenge that is too easy, they will feel bored. Both situations will give rise to users leaving your service."
    

# Data
categories: 
    - "faq"
    - "psychology"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/psychology/appropriate-challenges"
---

If we give new users a task that is too hard, they are going to feel stress and anxiety. If we give users a challenge that is too easy, they will feel bored... 

Both situations will give rise to users leaving your service. 

#### How to setup appropriate challenges

![](https://d2mxuefqeaa7sj.cloudfront.net/s_9EEF020C1264A33754EC2D027EEC939C3F3BCDB09B6D1C7B273F279EFBC23B28_1516631735436_image7.jpg)

##### Setup the objective
Finding a right challenges can be time-consuming. And usually require shift in thinking about user and service. Once executed right can lead to longer session times, easing of learning curve and gamification. 

##### Objective needs to addapt to time and skill
Once users complete more tasks, their skill level rises, making tasks previously experienced as difficult seem simple and effortless. This is why we need to provide a careful balance between the difficulty curve and the learning curve.

##### How I design for appropriate challenges
I mostly thinking about appropriate challenges as an sequence of events that progressively require an increased skill level. In video games, the events are often represented by levels; in e-learning, by lessons within courses. To complete a challenge, it is necessary that the requisite learning takes place.
