---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Direct Interaction"
description:
    - "An interface should imitate the user’s interaction with the physical world by having a direct correlation between user action and interface reaction."
    

# Data
categories: 
    - "faq"
    - "psychology"
tags: design
img: "/images/projects/guvernante/1.webp"
permalink: "faq/psychology/Direct-interaction"
---
An interface should imitate the user’s interaction with the physical world by having a direct correlation between user action and interface reaction. 

##### Directness
Means that the user is physically close to (touching) the interface he is interacting with. that interface actions happen at the same time as user actions or that the motions of elements on the interface follow the motions of the user. 

##### Contextual interaction
Sensesing the user motion, allowing them to interact with content on the screen via movements. The interaction is not close to the screen, but it responds in real time and follows the motions of the user.

##### High-frequency interaction
There is a constant flow of action and reaction between the user and the interface. If the user can constantly see the direct consequence of her actions, she is also receiving constant feedback. 

##### Avoid overwhelming the user
It is important that your interface also uses contextual interactions. Rather than showing all options at once. You should primarily show information that is relevant to the user’s current interaction. 