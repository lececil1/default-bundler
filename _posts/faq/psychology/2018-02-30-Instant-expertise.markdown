---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Instant Expertise"
description:
    - "If users can apply skills they have from other areas of their lives, you’ll save them the trouble of learning something completely new."
    

# Data
categories: 
    - "faq"
    - "psychology"
tags: psychology
img: "/images/projects/guvernante/1.webp"
permalink: "faq/psychology/instant-expertise"
---

Your interface you should take advantage of the user existing skills. If users can apply skills they have from other areas of their lives, you’ll save them the trouble of learning something completely new...

Once they realise what skills to use, the users can take advantage of their existing skills and expectations to interact with your interface. There are two ways of doing this:

##### Reusing common human skills
Refer to the things that most people know how to do—e.g., speaking. Design of interface should leaverage out of this. Common human skill is that you can assume that most of your users have the skill simply because they are human.

##### Domain-specific skills 
Skills of a specific user group.Users finds your user interface easier to use if it takes advantage of their existing knowledge rather than requiring them to learn something completely new.
