---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Law of Common Region"
description:
    - "Elements tend to be perceived into groups if they are sharing an area with a clearly defined boundary."
    

# Data
categories: 
    - "faq"
    - "psychology"
tags: psychology
img: "/images/projects/guvernante/1.webp"
permalink: "faq/psychology/Law-of-Common-Region"
---
Elements tend to be perceived into groups if they are sharing an area with a clearly defined boundary.

KEY TAKEAWAYS
Adding a border around an element or group of elements is an easy way to create common region.
Common region can be created by defining a background behind an element or group of elements.

ORIGINS
The principles of grouping (or Gestalt laws of grouping) are a set of principles in psychology, first proposed by Gestalt psychologists to account for the observation that humans naturally perceive objects as organized patterns and objects, a principle known as Prägnanz. Gestalt psychologists argued that these principles exist because the mind has an innate disposition to perceive patterns in the stimulus based on certain rules. These principles are organized into five categories: Proximity, Similarity, Continuity, Closure, and Connectedness.