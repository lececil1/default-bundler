---
# Config
layout: faq-detail
date:   2015-02-23 19:49:37 -0300
resource: true

# Info
title:  "Occam’s Razor"
description:
    - "Simplest solution is almost always the best.” With the flexibility and power of the web and our design tools, it is easy to get carried away. The result is a very complicated site or design that may have a lot of functionality and information, but is difficult to use, build and maintain."
    

# Data
categories: 
    - "faq"
    - "psychology"
tags: psychology
img: "/images/projects/guvernante/1.webp"
permalink: "faq/psychology/Occams-Razor"
---
Occam’s Razor put simply, states that “the simplest solution is almost always the best.” With the flexibility and power of the web and our design tools, it is easy to get carried away. The result is a very complicated site or design that may have a lot of functionality and information, but is difficult to use, build and maintain. 

##### More doesn't mean better
Despite the fact that one might think the site can do more, it actually accomplishes less. This is commonly an issue where companies feel the need to put everything they possibly could up on the website in the rare case that someone wants the information. What gets ignored is that the overwhelming majority of the users will access about 20%.

##### Focus on details not on quantity
Being ruthless about the value that a page or piece of content provides and removing anything that is unnecessary will make significantly stronger and more effective designs.

##### Simple means effective
A design isn’t finished when there is nothing more to add, but when there is nothing left to take away.” Design simplicity is elegant, sophisticated and much more effective than the complex decorative style.

