---
# Config
layout: portfolio-detail
date:   2017-02-23 19:49:37 -0300
excerpt_separator: <!--more-->

# Info
year: "2013"
client: "Button Systems"
title:  "Toonect - Product Identity"
description: "Developed a comprehensive brand identity for Toonect, a provider of low-maintenance call centers for small and medium businesses."

# Data
categories: 
    - "identity" 
    - "buttonsystems"
tags: identity
img: "/images/projects/toonect/1.webp"
hero: "/images/projects/toonect/Toonect-Instantly-Reachable.png"

# Gallery
img2: "/images/projects/toonect/2.webp"
imgdsc2: "Logo and Branding"
img3: "/images/projects/toonect-mobile/1.webp"
imgdsc3: "Mobile App Integration"

# URL
permalink: "ux-portfolio/identity/Toonect-Brand-Identity"
---

### Project Description:
Before diving into screen design, I focused on establishing a strong brand identity for Toonect. The project involved creating a logo and comprehensive branding elements to reflect the character and professionalism of their low-maintenance call center service.

### Goals:
- **Create a Unique Identity:** Develop a distinctive and versatile brand identity suitable for both digital and print media.
- **Ensure Recognizability:** Design branding elements that work across various scales, from large applications to small favicons.
- **Develop a Style Guide:** Create a design manual and style guide to maintain consistency.

### My Work:
- **Brand Identity:** Designed the logo, logotype, color scheme, and comprehensive design manual.
- **Product Integration:** Ensured that the identity seamlessly integrated into the product's digital and printed materials.

<!--more-->

### Results:
The brand identity created for Toonect successfully conveyed the character of their service and was effectively used across both digital platforms and printed materials. 

**Experience:** Brand Identity, Product Design  
**Software:** Adobe Illustrator, Adobe Photoshop

**Link:** [Toonect](http://www.toonect.com/)

**Industry:** Computers & IT, Data Services
