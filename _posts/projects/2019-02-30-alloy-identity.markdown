---
# Config
layout: portfolio-detail
date: 2019-02-23 19:49:37 -0300
excerpt_separator: <!--more-->

# Info
year: "2018"
client: "Yotta"
title:  "ALLOY - Identity"
description: "Connected Identity for Asset Management Software"

# Data
categories: 
 - "identity"
 - "yotta"
tags: identity
img: "/images/projects/product1.webp"
hero: "/images/projects/alloy/1.png"

# Gallery
img2: "/images/projects/alloy/6.webp"
imgdsc2: "Product identity"
img3: "/images/projects/alloy/2.webp"
imgdsc3: "Product identity"
img4: "/images/projects/alloy/8.webp"
imgdsc4: "Product identity"

# URL
permalink: "ux-portfolio/identity/Alloy-WebSoftware"
---

### Project Description:
Alloy provides asset managers with comprehensive insights into their networks, whether managing street lighting, roadworks, waste management, or public space maintenance.

### Goals:
- **Strong Identity:** Develop a compelling identity for Alloy that integrates seamlessly with Yotta’s existing brand.
- **Support Digital Transformation:** Enhance Yotta’s overall digital presence through effective visual communication.
- **Design Research:** Utilize research to select appropriate colors, shapes, and iconography.
- **Visual Language:** Create a cohesive icon-based language for the software and its various assets.
- **Cross-Platform Consistency:** Ensure the identity works across different platforms and mediums.

### My Work:
- **Product Design:** Designed the logo and established a comprehensive logo manual, including color palettes and design elements.
- **Design Manual:** Created a detailed brand and visual communication manual.
- **Brand Identity:** Developed a product identity that aligns with Yotta’s existing branding and enhances its overall visual appeal.
- **Icon Design:** Designed a set of icons for various modules within the software.

<!--more-->

### Results:
The product identity was integrated into Yotta’s existing branding, filling a crucial gap and reinforcing the company's visual language. The Alloy logo complements Yotta’s logo, symbolizing the "missing piece" in their service range.

### Achievements:
- **Public Reveal:** Launched the product identity to the public in mid-2016.
- **Visual Language:** Defined a comprehensive visual language, including colors, patterns, components, and a 500+ icon set.
- **Iconography:** Created symbols and icons for 20+ modules, including inspections, waste management, workflow builder, and layer builder.

### Review:
> Michal helped us take the quality of our design and UX to the next level on our SaaS product Alloy. Developing style guides, wireframes, and high-fidelity mockups to support the development team. Michal has a real eye for good design, and his experience with web technologies make him well-suited to working on both small and large-scale web apps.
CRAIG McNICHOLAS - Manager at Yotta

**Experience:** [Product Design](http://github.com/barryclark/jekyll-now/), [Brand Identity](http://github.com/barryclark/jekyll-now/)

**Software:** [Adobe Photoshop](http://github.com/barryclark/jekyll-now/), [Adobe Illustrator](http://github.com/barryclark/jekyll-now/)

**Link:** [Yotta - Alloy](http://www.weareyotta.com/)
