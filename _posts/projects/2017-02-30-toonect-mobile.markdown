---
# Config
layout: portfolio-detail
date:   2017-02-23 19:49:37 -0300
excerpt_separator: <!--more-->

# Info
title:  "TOONECT - Mobile App"
description: "Toonect Mobile provides a low-maintenance call center solution for small and medium businesses, enhancing communication efficiency and ease of use."

# Details
year: "2013"
client: "Button Systems"
categories: 
    - "mobile" 
    - "buttonsystems"
tags: mobile
img: "/images/projects/toonect-mobile2/1.webp"
hero: "/images/projects/toonect-mobile/Mobile-application-UI.png"

# Gallery
img2: "/images/projects/toonect-mobile/List-of-contacts.png"
imgdsc2: "Design of native application for PlayStore"
img3: "/images/projects/toonect-mobile/Application-dashobard.png"
imgdsc3: "Product identity for brand Toonect"
img4: "/images/projects/toonect-mobile/toonect-hp-pro.png"
imgdsc4: "My UX Design work was focused on ease of use and familiarity"
img5: "/images/projects/toonect-mobile/Toonect-mobile-application.png"
imgdsc5: "Mobile App UI and UX were simple but engaging"

# URL
permalink: "ux-portfolio/mobile/Toonect-Communication-in-Cloud"
---

### Project Description:
Toonect Mobile is a call center application designed for low maintenance, specifically targeting small and medium-sized businesses. My role involved designing the user experience in alignment with the brand identity, including crafting wireframes and high-fidelity prototypes.

### Goals:
- Create an intuitive mobile design with exceptional usability.
- Ensure seamless performance across both Android and iOS platforms.

### My Work:
- <b>UI/UX Design:</b> Collaborated closely with the product owner to deliver optimal design solutions and an efficient workflow.
- <b>Mobile Design:</b> Developed and tested multiple wireframes and prototypes to ensure compatibility and functionality on both Android and iOS devices.

<!--more-->

### Results:
The mobile application design was delivered to the client on schedule, including comprehensive design documentation, measurements, and exported assets.

<b>Experience:</b> Mobile Design, UI/UX  
<b>Software:</b> Adobe Illustrator, Adobe Photoshop, Balsamiq

<b>Link:</b> [Toonect Website](http://www.toonect.com/)

Computers & IT, Data Services
