---
# Config
layout: portfolio-detail
date:   2016-02-23 19:49:37 -0300
excerpt_separator: <!--more-->

# Info
title:  "Hotel Elisabeth - Mobile App"
description: "iPad application designed to enhance guest experience by promoting spa and restaurant services."

# Details
year: "2016"
client: "Begrip"
categories: 
    - "mobile"
    - "begrip"
tags: mobile
img: "/images/projects/elisabeth-mobile/1.webp"
hero: "/images/projects/elisabeth-mobile/1.webp"

# Gallery
img2: "/images/projects/elisabeth-mobile/2.webp"
imgdsc2: "Design of landing page to offer easy navigation for hotel guests"
img3: "/images/projects/elisabeth-mobile/3.webp"
imgdsc3: "Offers your direct access to all hotel services"

# URL
permalink: "ux-portfolio/mobile/Elisabeth-Historical-hotel-under-Trencin-Castle"
---

### Project Description:
Following the successful launch of Hotel Elizabeth’s website, the client requested an iPad application to complement their digital presence and promote the hotel's spa and restaurant services. 

### Goals:
- <b>Brand Consistency:</b> Design an app that reflects the identity of Hotel Elizabeth.
- <b>Online and Offline Functionality:</b> Ensure the app works seamlessly both online and offline.
- <b>Technical Solution:</b> Develop a solution that allows for easy updates via an admin dashboard.
- <b>Synchronization:</b> Ensure updates are synchronized effectively.

### My Work:
- <b>App Design:</b> Applied website design patterns to maintain consistency in navigation and orientation.
- <b>User Experience:</b> Created a user-friendly experience to enhance guest satisfaction with accessible information and historical content about the hotel.
- <b>Application Development:</b> Developed the app using PhoneGap, enabling it to update HTML content when online and serve pages from device memory when offline.
- <b>Scrum Master:</b> Managed an agile team, ensuring timely delivery and high-quality results through effective milestone tracking and issue resolution.

<!--more-->

### Results:
The app was developed and implemented within a week. It was successfully installed on all hotel devices and is actively used by guests to access information and services.

<b>Experience:</b> UI/UX, Mobile Design, HTML, CSS  
<b>Software:</b> Basecamp, InVision, Adobe Photoshop, PhoneGap

<b>Link:</b> [Available in Apple Store](#)
