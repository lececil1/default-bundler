---
# Config
layout: portfolio-detail
date:   2016-02-23 19:49:37 -0300
excerpt_separator: <!--more-->

# Details
year: "2015"
client: "Begrip"
title:  "Erste Bank - Good Savings"
description: "Micro-campaign for promoting a bank product, focusing on animation and interactive web elements."

# Data
categories: 
    - "motion"
tags: motion
img: "/images/projects/slps/1.webp"
hero: "/images/projects/slps/1.webp"

# Gallery
img2: "/images/projects/slps/2.gif"
imgdsc2: "Animation of page experience"

video: <iframe width="100%" height="220" src="https://www.youtube.com/embed/dUY-fmOz3Rg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# URL
permalink: "ux-portfolio/motion/Erste-bank-Good-savings"
---

### Project Description:
The micro-campaign for Erste Bank aimed to promote a new banking product through interactive web animations. While Elite Solutions handled the design, I focused on animation and Flash development to bring the campaign to life.

### Goals:
- **Increase Engagement:** Create an interactive web experience to raise interest among families about the bank's products.
- **High-Quality Animation:** Develop smooth, replicable animations to enhance user engagement.
- **Flash Development:** Implement ActionScript 3 to develop interactive navigation and animations.

### My Work:
- **Interaction Design:** Refined animations to ensure high-quality, responsive interactions.
- **Flash Development:** Scripted in ActionScript 3, creating interactive navigation based on a hopscotch game principle where each press triggered an event.

<!--more-->

### Result:
The project was delivered on a tight schedule, allowing Elite Solutions to launch the campaign on time. The interactive web experience successfully engaged users, with longer session times and positive feedback from the client.

**Experience:** 2D Illustration, Animation  
**Software:** Adobe Photoshop, Adobe Flash

**Link:** [Erste Bank - Good Savings](http://www.vynosnesporenie.sk/)

**Industry:** Marketing, Advertisement, Finance, Economy
