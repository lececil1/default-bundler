---
# Config
layout: portfolio-detail
date:   2015-02-23 19:49:37 -0300
excerpt_separator: <!--more-->

# Details
year: "2015"
client: "Begrip"
title:  "Guvernante - Web Design"
description: "Design and execution of visual communication for a high-end car rental service."

# Data
categories: 
    - "webdesign"
    - "begrip"
tags: webdesign
img: "/images/projects/guvernante/1.webp"
hero: "/images/projects/guvernante/1.webp"

# Gallery
img2: "/images/projects/guvernante/2.webp"
imgdsc2: "Mobile Optimization"
img3: "/images/projects/guvernante/4.webp"
imgdsc3: "Bold and Luxurious Design"

# URL
permalink: "ux-portfolio/webdesign/Guvernante-Rent-The-Race-Car"
---

### Project Description:
Guvernante, a high-end car rental service, sought a complete design solution to establish their brand identity and online presence. The project involved creating a visual identity, brand materials, and a responsive website.

### Goals:
- **Brand Identity:** Develop a luxurious and bold visual identity to represent the premium nature of the service.
- **Mobile-Friendly Web Design:** Create a mobile-optimized website to showcase the current range of vehicles.
- **Graphic Assets:** Design supporting brand materials and advertisements.

### My Work:
- **Identity Design:** Crafted a visual identity that reflects the luxury and exclusivity of the service.
- **Web Design:** Developed a clean, high-impact design focused on the details of the rental cars and user experience.
- **Front-End Development:** Implemented the design using modern web technologies for a seamless experience.
- **Graphic Design:** Designed promotional materials and advertisements to support the brand.

<!--more-->

### Result:
The final design was both bold and luxurious, capturing the essence of the high-end service. The website effectively showcased the rental cars and supported the brand with coherent communication materials. Ongoing support was provided post-launch to ensure brand consistency.

**Experience:** Identity Design, Web Design, UI/UX, HTML/CSS, Graphic Design  
**Software:** Adobe Illustrator, Adobe Photoshop, Adobe InDesign

**Link:** [Guvernante](http://www.guvernante.com/)

**Industry:** Auto-Moto, Rental
