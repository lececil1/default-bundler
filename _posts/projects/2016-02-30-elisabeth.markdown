---
# Config
layout: portfolio-detail
date:   2016-02-23 19:49:37 -0300
excerpt_separator: <!--more-->

# Info
title:  "Hotel Elisabeth - Web Design"
description: "Redesign of the web presence for Hotel Elizabeth, a prominent hotel in Slovakia."

# Details
year: "2015"
client: "Begrip"
categories: 
    - "webdesign"
    - "begrip"
tags: webdesign
img: "/images/projects/elisabeth/1.webp"
hero: "/images/projects/elisabeth/1.webp"

# Gallery
img2: "/images/projects/elisabeth/2.webp"
imgdsc2: "Mobile Optimization"
img3: "/images/projects/elisabeth/3.webp"
imgdsc3: "Pixel Perfect"

# URL
permalink: "ux-portfolio/webdesign/Hotel-under-a-medieval-castle"
---

### Project Description:
While working at Begrip, Hotel Elizabeth approached us to redesign their website to better represent their prominent status in Slovakia. My role involved both designing the site and managing its development to ensure a seamless experience.

### Goals:
- **Brand Representation:** Design a website that aligns with Hotel Elizabeth’s existing identity.
- **Cross-Platform Functionality:** Ensure the website is fully responsive and functional across all devices.
- **Simplified Administration:** Create an easy-to-use custom administration interface for non-technical staff.

### My Work:
- **Web Design:** Developed a design that included various modules and optimized for mobile use.
- **User Experience:** Focused on creating an enjoyable experience for both hotel clients and administrative staff.
- **Front-End Development:** Implemented a component-based design system to facilitate future updates and modifications.
- **Scrum Master:** Managed an agile team, overseeing development to ensure timely delivery and high-quality results.

<!--more-->

### Results:
The redesigned website underwent multiple iterations, resulting in improved accessibility and increased mobile traffic. The simplified administrative interface received positive feedback from staff for its ease of use.

**Experience:** UI/UX, Web Design, HTML, CSS, Front-End Development, Project Management  
**Software:** Basecamp, Adobe Illustrator, Adobe Photoshop, GIT

**Link:** [Hotel Elizabeth](http://www.hotelelizabeth.sk/)
