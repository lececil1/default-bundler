---
# Config
layout: portfolio-detail
date:   2019-02-23 19:49:37 -0300
excerpt_separator: <!--more-->

# Info
year: "2021 - 2024"
client: "7ofD"
title:  "IntoBridge.com - Online game of bridge"
description: IntoBridge brings the classic card game into the digital age, offering an immersive experience for professional bridge players. With more than 20,000 active players globally, IntoBridge allows users to play, laugh, learn, and challenge each other while socializing in real-time. I designed <b>branding, product, game, mobile app, marketing, and collaborated on front-end development.</b>
categories: 
    - "product" 
    - "7ofd"
img: "/images/projects/intobridge/intobridge-cover.png"
hero: "/images/projects/intobridge/intobridge.png"

# Gallery
imgdsc1: "Immersive experience for professional bridge players"
img2: "/images/projects/intobridge/1.png"
imgdsc2: "<b>Brand identity</b> Professional brand identity with passion for bridge"
img3: "/images/projects/intobridge/2.png"
imgdsc3: "<b>Application UI</b> Lobby service where you connect with other players"
img4: "/images/projects/intobridge/3.png"
imgdsc4: "<b>Game UI</b> Visuals and layout to appeal to professional bridge players"
img5: "/images/projects/intobridge/4.png"
imgdsc5: "<b>Optimization</b> Screens using adaptive approach and mobile UI patterns"
img6: "/images/projects/intobridge/5.png"
imgdsc6: "<b>Mobile application</b> with a focus on ranked games"
img7: "/images/projects/intobridge/6.png"
imgdsc7: "<b>Visual FX</b> Ranked games explanation of tiers"
img8: "/images/projects/intobridge/7.png"
imgdsc8: "<b>Player statistics</b> Integration of metrics raised engagement on platform"

# URL
permalink: "ux-portfolio/product-design/IntoBridge"
---

### Project description: 
IntoBridge modernizes the traditional card game with features like responsive gameplay, video chat rooms, daily tournaments, celebrity challenges, ranking systems, and UI gamification. As a major competitor in the market, IntoBridge continues to strengthen its position through ongoing technological advancements, attracting over 20,000 monthly players globally. 

<b>I led the design process</b> from the first sketches, working closely with the CEO, CTO, and development team to deliver high-quality results and improve the platform through iterative testing and continuous development. <b>I worked on the project for 2 years</b> from 2021 to 2024, as a principal designer. The game has attracted over 20,000 active players.

### My work:
- <b>Product Identity</b> - Logo, Logotype, Style Guide, Personas
- <b>Design System</b> - Patterns, Prototypes, Wireframes
- <b>Product UX</b> - Design, Prototypes, Development
- <b>Game design</b> - Animations, Interactions, Adaptability
- <b>Mobile App Design</b> - Prototypes, AppStore, PlayStore 
- <b>Front-End Lead</b> - LESS, Styleguide, Components, Design Tokens 
- <b>Marketing</b> - Producing high-fidelity advertisement with low friction 

<!--more-->

### Results: 
- Established a <b>Brand identity</b> for IntoBridge used in the application and marketing materials
- Developed a core <b>Game design</b> for online card play with video chat integration
- Designed a <b>UI/UX design</b> for the online lobby and all other pages, including gameplay
- Created <b>Visualizations and game animations, FX</b> for the bespoke ranking system and gameplay
- Led the design of UI/UX for the <b>Mobile game</b> IntoBridge
- Created high-conversion <b>subscription payment flows</b> and optimized onboarding screens
- Implemented <b>iterative design improvements</b> based on user feedback, enhancing user engagement
- Maintained a clean <b>Front-end</b> codebase and led code reviews
- Set up Figma environment and dependencies, <b>style guides, and design tokens</b>
- Managed <b>JIRA and Figma</b> projects to improve cross-team efficiency
- Produced <b>marketing materials and social media visuals</b> using AI Generative Design
- Delivered <b>Web design and brand assets</b> for IntoBridge.com
- Set up measurement statistics using <b>Google Analytics, Search Console, and Tag Manager</b>

<b>Experience:</b> Application design, Mobile Design, Design Systems, Workflows, UI/UX, HTML/CSS/React

<b>Software:</b> Figma (design systems, prototyping), Adobe XD (wireframing), Adobe Illustrator, Adobe Photoshop, MidJourney (AI design), Git, Jira
