---
# Config
layout: portfolio-detail
date:   2017-02-23 19:49:37 -0300
excerpt_separator: <!--more-->

# Info
year: "2012"
client: "Begrip"
title:  "Register Domain from Space"
description: "Campaign for the Slovak Space Program, featuring domain registration from near space."

# Data
categories: 
    - "motion"
    - "begrip"
tags: motion
img: "/images/projects/vesmirny/1.png"
hero: "/images/projects/vesmirny/1.png"

# Gallery
img2: "/images/projects/vesmirny/1.gif"
imgdsc2: "Animated parallax"
img3: "/images/projects/vesmirny/3.webp"
imgdsc3: "Out of planet"

video: <iframe width="100%" height="220" src="https://www.youtube.com/embed/YOUR_VIDEO_ID" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
permalink: "ux-portfolio/motion/Register-domain-from-Space"
---

### Project Description:
In April 2012, WebSupport, Slovakia's largest hosting provider, collaborated with the Slovak Organization for Space Activities to register web domains from near space. I was responsible for creating illustrations and animations for this unique campaign.

### Goals:
- **Engagement:** Achieve high levels of interaction on Twitter and other social networks.
- **Live Stream:** Attract viewers to watch the live stream of the event.
- **Sales Increase:** Boost domain sales during the event.

### My Work:
- **Illustrated Resources:** Developed graphical assets using a mix of 2D and 3D graphics, including a uniquely crafted SVG environment with a parallax effect.
- **Animations:** Created smooth, frame-by-frame animations optimized for performance on slower computers.

<!--more-->

### Results:
The project exceeded expectations, generating significant buzz on social media and the WebSupport website. Notably, WebSupport became the first company to sell domains from near space.

**Experience:** 2D Illustration, 3D Illustration, Animation, SVG  
**Software:** Adobe Illustrator, Adobe Flash, After Effects

**Link:** [Vesmírny Program](http://www.vesmirnyprogram.sk/)

**Tags:** Marketing, Advertisement, Technology, Research and Development
