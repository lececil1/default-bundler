---
# Config
layout: portfolio-detail
date:   2019-02-23 19:49:37 -0300
excerpt_separator: <!--more-->

# Info
title:  "ALLOY - Mobile App"
description: Alloy Mobile offers advanced, server-based mapping and Mesh functionality, seamlessly integrated with sensory-connected assets for real-time updates and efficient asset management.

# Details
year: "2015 - 2018"
client: "Yotta"
categories: 
 - "mobile"
 - "yotta"
tags: Mobile
img: "/images/projects/alloy-mobile2/2.webp"
hero: "/images/projects/alloy-mobile/Mobile-design.png"

# Gallery
img2: "/images/projects/alloy-mobile/Alloy-mobile-asset-management.png"
imgdsc2: "Branding of Alloy Mobile and design of interface patterns"
img3: "/images/projects/alloy-mobile/Whole-network-in-pocket.png"
imgdsc3: "Realtime native application of Alloy Mobile"
img4: "/images/projects/alloy-mobile/Full-connectivity.png"
imgdsc4: "Connection between desktop and tablet, mobile to be familiar"
img5: "/images/projects/alloy-mobile/Flexible-UI-runs-on-watchs.png"
imgdsc5: "Design of UI flexible enough to run on smartwatches"
img6: "/images/projects/alloy-mobile/Track-work-on-the-go.png"
imgdsc6: "Application featured on PlayStore"
img7: "/images/projects/alloy-mobile/Application-on-PlayStore.png"
imgdsc7: "Customer feeeback was very positive"

# URL
permalink: "ux-portfolio/mobile/Alloy-Connected-Asset-Management"
---

### Project Description:
Following the release of the Alloy WebApp, we recognized that the rest of the application suite within the Mayrise family appeared outdated. We embarked on a project to modernize the look and enhance usability. 

Through iterative design and testing, we consolidated multiple tablet, mobile, and PDA applications into a single, unified mobile experience. This new design, optimized for both Android and Apple devices, utilizes updated design patterns and software environments. Alloy Mobile now offers rich server-based mapping and Mesh functionality, fully integrated with the Alloy platform to provide real-time updates on asset statuses and maintenance tasks.

### Goals:
- Develop a <b>scalable and adaptive design</b> based on the new Alloy identity.
- Streamline existing user flows and incorporate new functionalities.
- Align interface and user patterns with the updated Alloy software.
- Expand user capabilities with additional features and improved functionality.

### My Work:
- <b>Mobile Design:</b> Crafted a user experience that mirrors the web app, utilizing Material Design principles for consistency and ease of use.
- <b>Prototyping:</b> Conducted numerous rapid prototypes to find the most efficient design solutions.
- <b>Interaction Design:</b> Ensured discoverability and a smooth learning curve for the app’s extensive functionality.
- <b>UI/UX:</b> Designed for compatibility with a range of devices, including low-end models and smartwatches, with a user interface adaptable to displays as small as 300px.

<!--more-->

### Results:
The redesigned application has become a central tool for asset inspectors, continually updated with new modules and functionalities. Over the course of one and a half years, I developed an app rich with features while maintaining flexibility to accommodate diverse asset management scenarios.

### Review:
> Working with Michal at Yotta was a pleasure. He consistently delivered high-quality designs that were not only visually appealing but also easy to implement. His deep understanding of mobile products and thoughtful approach to design were evident in the positive user feedback we received. Michal's core ideas and design principles continue to influence the app's look and functionality, even after several years. I highly recommend Michal for his talent, dedication, and visionary approach.
Sylwester Moniuszko-Szymanski - Mobile Developer at Yotta

<b>Experience:</b> UI/UX, Material Design, Mobile Design, Interactions, Prototyping  
<b>Software:</b> Adobe XD

<b>Link:</b> Available on Play Store and Apple Store.
