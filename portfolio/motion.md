---
# Config
layout: portfolio-posts
resource: true

# Info
title:  "Motion Design"
description: Crafting interactive animations that elevate user experience and breathe life into your brand across digital platforms.
nav-title: "Motion"

# Data
categories: motion
img: "/images/projects/motion1.webp"
imgdsc1: "Animations and interactions"
img-2: "/images/projects/motion2.webp"
permalink: "/ux-portfolio/motion-design/"
---

I create interactive animations that bring your brand’s identity to life, enhancing digital experiences. Whether refining user interfaces or crafting compelling social media content, motion design helps your brand:
- <b>Boost User Engagement:</b> Dynamic interactions that capture attention.
- <b>Tell Your Brand’s Story:</b> Compelling visual narratives that resonate with your audience.
- <b>Enhance UI/UX:</b> Adding fluidity and intuitiveness to digital products for a seamless experience.
