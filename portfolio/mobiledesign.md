---
# Config
layout: portfolio-posts
resource: true

# Info
title: "Mobile Design"
description: "Design of interactive mobile apps and games for Android and iOS. Simple, functional, and attractive native apps for all devices."
nav-title: "Mobile"

# Data
categories: mobile
img: "/images/projects/mobile1.webp"
imgdsc1: "Design of positive experiences during the use of mobile devices, wearables, and applications."
img-2: "/images/projects/mobile2.webp"
permalink: "/ux-portfolio/mobile-design/"
---

I design interactive mobile apps and games for both Android and iOS, focusing on creating simple, functional, and visually appealing native apps. My approach ensures:
- <b>Seamless User Experience:</b> Delivering intuitive, fluid interactions across devices.
- <b>Tailored Designs:</b> Creating apps that meet the unique needs of each platform and device.
- <b>Attractive Interfaces:</b> Designing visually engaging experiences that captivate users.
