---
# Config
layout: company-detail
resource: true

# Info
title: "Senior UX Designer & Front-End Developer"
description: 
    - "Responsible for the design and development of Baxi Heating Brands. Replaced external agencies, allowing the entire solution to be managed in-house, which significantly improved delivery time and efficiency. All activities were thoroughly documented, researched, and implemented, contributing to faster and more effective delivery compared to external agencies."

# Details
year: "2021 - 2020"
client: "Baxi"
logo: "/images/logos/baxi.webp"
about: 
    - "Baxi is a leading UK heating brand, offering reliable heating and hot water solutions to homes and businesses. As part of their digital transformation, I helped them unify design and development efforts under one roof."

projects: 
    - "Revolutionizing the design and development for all Baxi Heating Brands by consolidating external efforts into an in-house operation. This allowed for faster delivery, better communication, and more cohesive design."
responsibilities: 
    - "Product Design" 
    - "Web & Mobile Design"
    - "UI/UX" 
    - "Prototyping" 
    - "Wireframes"
    - "Front-end development using Sitecore"
    - "BrowserStack for internal communication tools"
    - "Brand page maintenance and innovation"
excerpt:
    Senior UX Designer and Front-End Sitecore Developer
    <li> Product Design, UI/UX, Prototyping, and Front-End Development</li>
 
# Review
review:
    - "Michal's work for Baxi transformed our approach to design and development. He led the effort to bring all design and development in-house, replacing external agencies, which allowed us to streamline our workflow and deliver faster. His expertise in UI/UX and Sitecore development was crucial to our success."
review-name:
    - "Baxi Team"

# Data
categories: baxi
tags: uxdesign
img: "/images/projects/web1.webp"
imgdsc1: "Baxi Heating Brands"
img-2: "/images/projects/web2.webp"
permalink: "ux-design-resume/baxi/"
---
<h4>Design and Development of Baxi Heating Brands</h4>
<ul>
    <li> Brought design and development under one roof, replacing external agencies</li>
    <li> Developed brand pages using Sitecore</li>
    <li> Created internal communication tools using BrowserStack technologies</li>
    <li> Documented, researched, and delivered prototypes and redesigns</li>
    <li> Faster delivery and better design ownership</li>
</ul>
<br/>
<h4>Tools and Technologies</h4>
<ul>
    <li> Sitecore, HTML, CSS, BrowserStack</li>
</ul>
