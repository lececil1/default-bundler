---
# Config
layout: company-detail
resource: true

# Info
title: "Principal UI & UX Designer"
description: 
    - "UI and UX design for web and mobile applications. The software offers VoIP services with a focus on the B2B market, allowing mid-size companies to set up a professional call-centre. I created an intuitive and user-friendly design, using visual purity and a minimalistic approach across all platforms and devices. I have gained valuable experiences in teamwork and developed high-quality products."

# Details
year: "2013 - 2011"
client: "Button Systems"
logo: "/images/logos/bs.webp"
about: 
    - "Button Systems is a VoIP communication provider. Focusing on providing low-cost and low-maintenance call centres for small and medium businesses. Services include voice, instant messaging, email, fax, PBX, call centres, and web hosting."

projects: 
    - "Toonect is a solution for smaller businesses to afford professional call centre services."
responsibilities: 
    - "Product Design" 
    - "Web/Mobile Design"
    - "UI/UX" 
    - "Prototyping" 
    - "Wireframes"
    - "GUI"
    - "CI"
    - "Visual Communication"
excerpt:
    Product Design, Web/Mobile Design
    <li> UI/UX, Prototyping, Wireframes, GUI"
    <li> CI and Visual Communication"

# Data
categories: buttonsystems
tags: webdesign
img: "/images/projects/web1.webp"
imgdsc1: "UI and UX design for web and mobile applications."
img-2: "/images/projects/web2.webp"
permalink: "ux-design-resume/buttonsystems/"
---

<h4>Design of Identity Toonect</h4>
<ul>
    <li>Lead brainstorms with the research and consultancy team</li>
</ul>

<h4>Design a Responsive Web Product Site</h4>
<ul>
    <li>Concept direction</li>
    <li>2016 Launch of the page on the global market</li> 
</ul>

<h4>Design of Mobile App</h4>
<ul>
    <li>Unique and intuitive design</li>
    <li>Worked closely with the product owner to deliver the best results</li>
    <li>2017 Release of iOS, Android App</li>
</ul>
