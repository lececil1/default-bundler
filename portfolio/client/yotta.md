---
# Config
layout: company-detail
resource: true

# Info
title: "Principal Senior UX Designer & Product Designer"
description: 
    - "Design based on usability, accessibility, and user-experience. Responsive layout, flexible grid, and development. I designed software usability and identity, testing hundreds of designs and user stories. This led to the launch of Mayrise Mobile, Alloy, and Alloy Mobile — a mobile-friendly design with broad modularity, equipped with astonishing data visualizations."

# Details
year: "2018 - 2015"
client: "Yotta"
logo: "/images/logos/yotta.webp"
about: 
    - "Yotta is a connected asset management software and services provider. We empower organizations to make better, more informed decisions by ensuring our customers' systems, assets, and people are connected."
projects: 
    - "Alloy takes asset-rich environments into the future with connected asset management. Alloy is the only connected asset management solution able to seamlessly connect people, systems, and assets."
responsibilities: 
    - "Product Design"
    - "Ideation"
    - "Web & App Design"
    - "Design Lead"
    - "UI/UX"
    - "Prototyping"
    - "Wireframes"
    - "Interface Design"
    - "GUI" 
    - "Data Visualization"
    - "Map Design" 
    - "Interaction Design" 
    - "Front-end Development"
    - "Usability Testing"
    - "Design Documentation"
short-desc:
    - "I designed Alloy software's usability and identity. The mobile-friendly design, broad modularity, and astonishing data visualizations were key characteristics of my work. The new interface and functionality led Yotta to launch Mayrise Mobile, Alloy, and Alloy Mobile."
excerpt:
    Product Design, Ideation, Web & App Design
    <li> UX, Prototyping, Wireframes, Interface Design </li>
    <li> Visualizations, Maps, Interactions </li>
    <li> Front-end Development </li>

# Review
review:
    - "Michal helped us take the quality of our design and UX to the next level on our SaaS product Alloy. Developing style guides, wireframes, and high fidelity mockups to support the development team. Michal has a real eye for good design, and his experience with web technologies makes him well-suited to working on small to large-scale web apps."
review-name:
    - "CRAIG McNICHOLAS | Manager at Yotta Ltd"

# Data
categories: yotta 
tags: webdesign
img: "/images/projects/web1.webp"
imgdsc1: "Alloy takes asset-rich environments into the future with connected asset management."
img-2: "/images/projects/web2.webp" 
permalink: "ux-design-resume/yotta/"
---
<h4>Product Design of Alloy and Visual Language</h4>
<ul>
    <li>Design the look and feel of the product</li>
    <li>Creating a product identity</li>
    <li>Usability and accessibility</li>
    <li>Experience, prototyping, and ideation</li>
    <li>User testing and personas</li>
</ul>
<br/>

<h4>Design and Release of Alloy WebApp</h4>
<ul>
    <li>Created branded visual communication</li>
    <li>Design of interface and interaction</li>
    <li>Delivered more than 15 modules total</li>
    <li>Data and map visualization, structure modeling</li>
    <li>2017 launch of Alloy was greeted with standing ovations</li>
</ul>
<br/>

<h4>Design and Launch of Alloy Mobile</h4>
<ul>
    <li>2016 update to Mayrise applications</li>
    <li>Updated design for the applications</li>
    <li>Designed Alloy Mobile to merge all apps into one</li>
    <li>2018 Alloy Mobile delivered on Google Play and App Store</li>
</ul>
