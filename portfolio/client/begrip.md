---
# Config
layout: company-detail
resource: true

# Info
title: "Senior UI & UX Designer & Creative Art Director"
description: 
    - "In charge of ideation, business planning, web & app design, marketing, eCommerce, visual identity, and brand strategy for clients. Focused on every detail of the projects, from the initial concept to final product delivery in both online and offline channels."

# Details
year: "2016 - 2011"
client: "Begrip s.r.o."
logo: "/images/logos/begrip.webp"
about: 
    - "A design studio in the heart of Bratislava, delivering online solutions with a focus on campaigns and web projects."

projects: 
    - "Microcampaigns, advertising, websites, and animation for various European clients. I worked on every phase of these projects from ideation to pixel-perfect delivery and development."
responsibilities: 
    - "Project Management"
    - "Marketing" 
    - "Task Management"
    - "Deadlines"
    - "Ideation"
    - "Web App Design" 
    - "UI/UX" 
    - "Prototyping"
    - "Front-end Development"
    - "Usability" 
    - "Testing" 
    - "SCRUM Master"
excerpt:
    Ideation, Web & App Design, UI/UX, Prototyping
    <li> Project Management, Task Management</li>
    <li> Front-end Development, Usability, SCRUM</li>

# Data
categories: begrip
tags: webdesign
img: "/images/projects/web1.webp"
imgdsc1: "Microcampaigns, advertising, websites, and animation for various European clients."
img-2: "/images/projects/web2.webp"
video: <iframe width="100%" height="260" src="https://www.youtube.com/embed/SbTX50mQboU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
permalink: "ux-design-resume/begrip/"
---

<h4>Design Websites, Mobile Apps, and Creative Campaigns for Clients</h4>
<ul>
    <li>Projects delivered ahead of schedule</li>
    <li>Transparent communication with developers</li>
</ul>

<h4>Systems</h4>
<ul>
    <li>PHP, JAVA, HTML, LESS, WordPress, PrestaShop</li>
</ul>

<h4>Tools</h4>
<ul>
    <li>Adobe Master Collection, Photoshop, Illustrator, InVision</li>
</ul>
