---
# Config
layout: company-detail
resource: true
 
# Data
title: "Teacher - Design and Marketing for Digital Media"
description: 
    - "<b>Teacher of online marketing, digital design, and production</b>. With my students, we designed, developed, and delivered campaigns, websites, e-shops, social marketing, and other projects."

# Details
year: "2011 - 2010"
client: "Polygraficka"
about: 
    - "The Secondary Vocational School of Graphic Arts is the only one of its kind in Slovakia, offering specialized programs in polygraphic and graphic studies. The school also provides post-secondary courses and professional services in printing production."

projects: 
    - "Tutored students in digital media tools, design principles, and marketing. I worked with six different classes, from ages 14 to 30, in 4-day sessions every two weeks, teaching for 8 hours daily."
responsibilities:
    - "Teaching and mentoring"
    - "Web and App Design" 
    - "UI/UX Design"
    - "Marketing"
    - "Adobe Master Collection" 
    - "WordPress and CMS Management"
    - "Motion Design" 
    - "ActionScript" 
    - "Front-End Development"
excerpt:
    Web & App Design, <b>UI/UX Design</b>, Marketing
    <li> <b>Adobe Master Collection</b>, <b>WordPress</b>, <b>CMS Management</b></li>
    <li> <b>Motion Design</b>, <b>ActionScript</b>, <b>Front-End Development</b></li>

# Data
categories: sos
tags: webdesign
img: "/images/projects/web1.webp"
imgdsc1: "Teacher of online marketing, digital design, and production."
img-2: "/images/projects/web2.webp"
permalink: "ux-design-resume/polygraficka/"
---

<h4>Conducting Experiments and Implementing Results for Client Projects</h4>
<ul>
    <li>All students learned the basics of programming, design, and animation.</li>
    <li>Students gained hands-on experience with industry-standard creative tools.</li>
</ul>

<h4>Systems</h4>
<ul>
    <li>PHP, HTML, CSS, ActionScript 3, WordPress</li>
</ul>

<h4>Tools</h4>
<ul>
    <li>Adobe Master Collection, Flash, Photoshop, Illustrator, InDesign, Fireworks, Dreamweaver, Premiere, Vegas, Blender, Rhino3D, and more...</li>
</ul>
