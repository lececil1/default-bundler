---
# Config
layout: company-detail
resource: true

# Info
title: "Product Designer & Principal UX Designer"
description: 
    - "Worked on the branding, product design, mobile app design, marketing, front-end development, and game UI design for IntoBridge — an online game of bridge. I focused on creating an engaging and user-friendly interface, while also ensuring the brand identity was cohesive across various digital platforms."

# Details
year: "2024 - 2021"
client: "7ofD"
logo: "/images/logos/7ofd.webp"
about: 
    - "7ofD is a company focused on developing and launching innovative gaming products, with a particular focus on online and mobile platforms. Their flagship product, IntoBridge, aims to bring the traditional game of bridge into the digital age, providing a modern, accessible way for players to enjoy the game."
projects: 
    - "IntoBridge is an online platform for playing the card game bridge, with a focus on accessibility, engagement, and community building. The platform supports both mobile and web applications, delivering a seamless user experience across devices."
responsibilities: 
    - "Branding"
    - "Product Design"
    - "UI/UX Design"
    - "Mobile App Design"
    - "Marketing"
    - "Front-end Development"
    - "Game UI Design"
short-desc:
    - "My work on IntoBridge involved full-spectrum design responsibilities, from branding and product design to UI/UX and game interface development. I also contributed to marketing strategies and front-end development, ensuring a cohesive and engaging user experience across web and mobile platforms."
excerpt:
    Branding, Product Design, UI/UX Design
    <li> Mobile App Design, Marketing, Game UI</li>
    <li> Front-end Development</li>

# Data
categories: 7ofd
tags: webdesign, game-design
img: "/images/projects/intobridge1.webp"
imgdsc1: "IntoBridge is an online platform for playing bridge."
img-2: "/images/projects/intobridge2.webp" 
permalink: "ux-design-resume/7ofd/"
---
<h4>Branding and Product Design for IntoBridge</h4>
<ul>
    <li>Developed a cohesive brand identity for IntoBridge</li>
    <li>Designed a visually appealing and intuitive user interface</li>
    <li>Ensured consistency in visual language across web and mobile platforms</li>
</ul>
<br/>

<h4>Mobile App and Web Design</h4>
<ul>
    <li>Created responsive designs for both web and mobile applications</li>
    <li>Focused on user experience optimization across devices</li>
    <li>Launched a fully functional mobile app for bridge players</li>
</ul>
<br/>

<h4>Game UI Design and Front-end Development</h4>
<ul>
    <li>Designed interactive and user-friendly game interfaces</li>
    <li>Led front-end development efforts for the IntoBridge platform</li>
    <li>Collaborated with developers to ensure smooth gameplay and performance</li>
    <li>Developed marketing materials to support the platform's launch</li>
</ul>
