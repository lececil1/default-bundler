---
# Config
layout: faq-category
resource: true

# Info
title: "Development"
description: 
    - "I have been in charge of ideation, business planning, web & app design, marketing, eCommerce, visual identity and brand strategy for our clients. Focusing on every detail of the client projects, from the beginning to the final product in both online and offline world."
 
# Data
categories: code
tags: webdesign
img: "/images/projects/web1.webp"
permalink: "/faq/development/"
---

<h2>Front-End Development</h2>

<h5>My work</h5>
<p>Front-end, Themes, Layouts, Components, Documentation, UI Kit, Templates, Mobile optimalization, Accessibility, Usability, Testing, Monitoring
</p>

<h5>Tools</h5>
<ul class="skill"><li>React</li><li>HTML</li><li>CSS3</li><li>GIT</li><li>WordPress</li><li>Jekyll</li><li>Prestashop</li><li>Drupal</li><li>AS3</li><li>Shopify</li></ul>