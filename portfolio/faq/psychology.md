---
# Config
layout: faq-category
resource: true

# Info
title: "Psychology"
description: 
    - "I have been in charge of ideation, business planning, web & app design, marketing, eCommerce, visual identity and brand strategy for our clients. Focusing on every detail of the client projects, from the beginning to the final product in both online and offline world."
 
# Data
categories: psychology
tags: webdesign
img: "/images/projects/web1.webp"
permalink: "/faq/psychology/"
---

<h2> {{ page.title }} </h2>

<h5>My work</h5>
<p>UX Design, Research, User testing, Analisys, Personas, User journey, Data Analitics, A/B Testing
</p>

<h5>Tools</h5>
<ul class="skill"><li>Testing</li><li>Google Analytics</li><li>Google Tag Manager</li><li>InVision</li><li>Google Trends</li><li>Social networks</li><li>Facebook Insights</li><li>Trello</li></ul>