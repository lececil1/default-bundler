---
# Config
layout: faq-category
resource: true

# Info
title: "Management"
description: 
    - "I have been in charge of ideation, business planning, web & app design, marketing, eCommerce, visual identity and brand strategy for our clients. Focusing on every detail of the client projects, from the beginning to the final product in both online and offline world."
 
# Data
categories: management
tags: management
img: "/images/projects/web1.webp"
permalink: "/faq/management/"
---

<h2> {{ page.title }} </h2>

<h5>My work</h5>
<p>Strategy, Planning, Targets, Deadlines, Communication, Meetings, Workshops, Stakeholders, Ideation
</p>

<h5>Tools</h5>
<ul class="skill"><li>Slack</li><li>Basecamp</li><li>JiRA</li><li>GitLab</li><li>Atlassian</li><li>SCRUM</li><li>AGILE</li><li>Trello</li></ul>