---
# Config
layout: faq-category
resource: true

# Info
title: "Design"
description: 
    - "I have been in charge of ideation, business planning, web & app design, marketing, eCommerce, visual identity and brand strategy for our clients. Focusing on every detail of the client projects, from the beginning to the final product in both online and offline world."
 
# Data
categories: design
tags: webdesign
img: "/images/projects/web1.webp"
permalink: "/faq/design/"
---

<h2>Design</h2>

<h5>My work</h5>
<p>Product identity, Ideas, Concepts, Storyboards, Wireframes, Prototypes, Gui, UX, Visual design, Graphic design, Branding
</p>
My projects include <b>IntoBridge</b> (game UI design), <b>Alloy for Yotta</b> (SaaS product design), and various web/mobile design and development projects.</p>

<h5>Tools</h5>
<ul class="skill"><li>Figma</li><li>Adobe XD</li><li>Photoshop</li><li>Sketch</li><li>Adobe Animate</li><li>Adobe Master Collection</li><li>Blender 3D</li><li>Premiere</li><li>MapBox</li><li>Fontastic</li><li>Illustrator</li><li>Jira</li><li>Git</li>
    </ul>