---
# Config
layout: portfolio-posts
resource: true

# Info
title:  "Web design"
description: UX Design of e-shops, corporate sites, blogs and products. Accessible, responsive and user-friendly experience.
nav-title: "Web"

# Data
categories: webdesign
img: "/images/projects/web1.webp"
imgdsc1: "Design based on usability, accessibility and user-experience. Responsive layout, flexible grid and development."
img-2: "/images/projects/web2.webp"
permalink: "/ux-portfolio/web-design/"
---
I design seamless, accessible, and responsive websites that enhance user experience across devices. From e-shops to corporate sites, blogs, and digital products, my focus is on creating user-friendly, engaging experiences that drive results:
- <b>User-Centered Design:</b> Ensuring intuitive, easy-to-navigate layouts.
- <b>Responsive & Accessible:</b> Optimized for all devices and inclusive for every user.
- <b>Engaging Experiences:</b> Fostering interaction and retention through thoughtful design.