---
# Config
layout: articles

# Info
title: Offline
description: We are sorry the page you looking for is not available. Please check your internet connection. 

# Data
img: "/images/projects/product1.webp"
permalink: /offline/
---
<section>
    <h2 style="width:100%;">Whoops! You are offline.</h2>
    <a class="button button-blue button-fluid" href="/" title="UXMichal Discover">Try Refresh</a>
</section>