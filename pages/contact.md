---
# Config
layout: articles

# Info
title: Let's Build Your Next Great Digital Experience Together
description: Have a project in mind or need help enhancing your digital presence? Get in touch with me today to discuss how I can help turn your ideas into intuitive, impactful designs that drive results. Let’s collaborate and create something remarkable—click here to start the conversation!

# Gallery
img2: "/images/projects/bbq/2.webp"
imgdsc2: "Mobile Optimization"

# Data
img: "/images/projects/product1.webp"

permalink: /contact-your-designer/
---
<section class="row">
  <div class="col-sm-12 text">
      <div style="background:#1e82ff;padding:20px;color:#FFF; ">
          <div class=" col-sm-6 row">
                <div class="col-md-2">
                </div>
                <div class="col-md-7"> 
                    <h2>UX Michal – Let's Create Delightful Experiences Together</h2>
    <p>I’ve partnered with businesses like IntoBridge, Toonect, and Yotta to bring their products to life with engaging, user-centered design. From crafting strong brand identities to developing immersive web and mobile apps and leading UX for complex SaaS platforms, I’m committed to delivering exceptional user experiences every time. <br/><br/><h4>Ready to make something impactful?</h4></p>       
    <section>
      <a class="button button-white" style="max-width:600px;" href="https://linkedin.com/in/lececil">Connect on LinkedIn</a>
      Or email me at: <a class="text-white" style="color: rgb(29, 63, 105);" href="mailto:info@lececil.com">info@lececil.com</a>. Location: EU / UK / Remote
    </section>
    </div>
           <div class="col-md-2"></div>
    </div>
    </div>
      <br /><br /><br />
    <div class="col-sm-12">
        <h2>What Clients Say</h2>
        <div class="row" style="padding: 0;">
            <div class=" col-sm-6 row">
                <div class="col-sm-2">
                    <img src="/images/assets/craig.png" width="60px" height="60px" style="border-radius: 100%"/>
                </div>
                <div class="col-sm-9" style="background:#ededff;padding: 10px;"> 
                    Michal helped us take the quality of our design and UX to the next level on our SaaS product Alloy. Developing style guides, wireframes, and high-fidelity mockups to support the development team. Michal has a real eye for good design and his experience with web technologies make him well suited to working on small to large-scale web apps.<br /> 
                    <b>CRAIG McNICHOLAS <br/> Manager at Yotta</b>
                </div>
            </div>
            <div class="row col-sm-6">
                <div class="col-sm-2">
                    <img src="/images/assets/manish.png" width="60" height="60" style="border-radius: 100%"/>
                </div>
                <div class="col-sm-9" style="background:#ededff;padding: 10px;">
                    Michal was instrumental in the early design of Alloy, a highly flexible and innovative application set to change the way infrastructure assets are managed. Starting with a high-level brief about what we wanted to achieve, he was able to bring the application to life through a series of sketches and prototypes. He has a natural flair and passion for design which he is able to put to use in a wide range of applications. <br />
                    <b>MANISH JETHWA - CTO at Yotta</b>
                </div>
            </div>
        </div>
    </div>

