---
# Config
layout: resume

# Info
title: Over 15 Years of UX Design and Product Development Expertise
description: Explore my journey as a UX designer with over 15 years of experience in product design, prototyping, and front-end development. From SaaS platforms to mobile apps, discover the key roles I've played and the skills I've honed across various industries. See how my expertise can bring value to your next project.

# Data
categories: Resume
img: "/images/projects/product1.webp"
permalink: /ux-design-resume/
---
