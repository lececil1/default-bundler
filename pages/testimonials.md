---
# Config
layout: articles

# Info
title: Testimonials
description: Reach your business goals with excellent user experience. Let’s start working on your awesome web or iOS app! #Product identity, #Ideas, #Concepts, #Storyboards, #Wireframes, #Prototypes, #Gui, #UX, #Visual design, #Graphic design, #Branding... Discover more 

# Data
categories: Testimonials
img: "/images/projects/product1.webp"
permalink: /testimonials/
---

<section class="row">
<h2 class="col-sm-12">{{ page.title }}</h2>
    <div class="col-sm-4  col-xs-12">
            <strong>CRAIG McNICHOLAS</strong>
            <p>Manager at Yotta Ltd</p>
    </div>
    <div class="col-sm-8  col-xs-12">
            <blockquote>Michal helped us take the quality of our design and UX to the next level on our SaaS product Alloy. Developing style guides, wireframes and high fidelity mockups to support the development team. Michal has a real eye for good design and his experience with web technologies make him well suited to working on small to large scale web apps. </blockquote>
            <br style="margin: 20px 0;">
    </div>
    <div class="col-sm-4  col-xs-12">
            <strong>Sylwester Moniuszko-Szymanski</strong>
            <p>Mobile Application Developer at Yotta Ltd</p>
    </div>
    <div class="col-sm-8  col-xs-12">
            <blockquote>I worked with Michal at Yotta. Exceptional is the fact that he understood Mobile product and he was always delivering designs that not only looked nice but was easy to implement. From user perspective we got plenty of positive feedback about usability. He was also just great person to talk especially about cool stuff that will have impact on future and technology. I will recommend Michal for anyone that is looking for talent, hard work, understanding, and visionary.The best proof of Michal skill is the fact that after few years and designers that came after him the core ideas and look and feel he introduced are still there.</blockquote>
            <br style="margin: 20px 0;">
    </div>
    <div class="col-sm-4  col-xs-12">
            <strong>Sonja Nase Strugar</strong>
            <p>Senior Software Engeneer at Yotta Ltd</p>
    </div>
    <div class="col-sm-8  col-xs-12">
            <blockquote>I worked with Michal as a front end developer at Yotta, implementing his designs into our Alloy product. Michal joined the team soon after we started development and has worked hard and long hours to get everything ready for us. His work made huge difference to the intuitive and clean look of the product. People were really impressed during the first demo. It was pleasure to cooperate with Michal and I would enjoy working with him again.</blockquote>
            <br style="margin: 20px 0;">
    </div>
    <div class="col-sm-4  col-xs-12">
            <strong>Shivani Prashar</strong>
            <p>Communications Professional</p>
    </div>
    <div class="col-sm-8  col-xs-12">
            <blockquote>Michal has a very keen eye for detail and understands the the world of UX extrememly well. For me, he was a great support during the user persona and content write up for Yotta's cutting-edge, connected asset management platform - Alloy. Michal is an asset to any team; highly recommended.</blockquote>
    </div>
</section>