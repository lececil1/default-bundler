---
# Config
layout: articles

# Info
title: Transforming Visions into Impactful Digital Experiences that Drive Results
description: I’m passionate about turning ideas into impactful, user-centered designs that not only look great but deliver results. Whether it’s a mobile app, website, or interactive platform, I craft experiences that engage and convert. Ready to take your project to the next level? Click to see how we can make it happen.

# Data
---

<section style="background:#ededff;background:linear-gradient(rgba(237,237,255,.9),rgba(237,237,255,.9)),url(/images/assets/space.webp);background-size:100% auto;">
    <div class="row">
        <div class="col-sm-4 col-xs-3">
            <img class="img-nopadding display-notmobile" src="/images/assets/uxmichal-your-ux-designer.svg" alt="Your product designer UXMichal" style="width:200px;"/>
        </div>
        <div class="col-sm-7 col-xs-12" style="display: flex;flex-direction: column;flex-wrap: nowrap;justify-content: center;">
            <span class="font-blue">Meet UX Designer Michal</span>
    <h2>I create products that make users happy and help your business grow.<br/> </h2>
    <p>From initial idea to final product, I am passionate about crafting designs with purpose. Collaboration is key to delivering something remarkable and tangible that truly stands out.</p>
    <ul class="checkmarks">
        <li>Innovative and user-centered UX</li>
        <li>15+ years of product development</li>
        <li>Expertise in prototyping and design</li>
        <li>Collaborative, results-driven approach</li>
    </ul>
    <a class="button button-blue button-fluid" href="/what-design-i-do/" title="My design approach to product and UX">Let’s Get Started</a>
    <br/>
</div>
    </div>
</section>

<section>
    {% include components/index-expertise.html %}
</section>

<section style="background:#ededff;">
    {% include components/index-experience.html title="My Experience" %}
</section>
<section>
    
<div class="col-sm-12">
        <div class="row">
             <div class="col-sm-12">
    <h2>What Clients Say</h2>
    </div>
            <div class=" col-sm-6 row">
                <div class="col-sm-2">
                    <img src="/images/assets/craig.png" width="60px" height="60px" style="border-radius: 100%"/>
                </div>
                <div class="col-sm-9" style="background:#ededff;padding: 10px;"> 
                    Michal helped us take the quality of our design and UX to the next level on our SaaS product Alloy. Developing style guides, wireframes, and high-fidelity mockups to support the development team. Michal has a real eye for good design and his experience with web technologies make him well suited to working on small to large-scale web apps.<br /><br /> 
                    <b>CRAIG McNICHOLAS <br/> Manager at Yotta</b>
                </div>
            </div>
            <div class="row col-sm-6">
                <div class="col-sm-2">
                    <img src="/images/assets/manish.png" width="60" height="60" style="border-radius: 100%"/>
                </div>
                <div class="col-sm-9" style="background:#ededff;padding: 10px;">
                    Michal was instrumental in the early design of Alloy, a highly flexible and innovative application set to change the way infrastructure assets are managed. Starting with a high-level brief about what we wanted to achieve, he was able to bring the application to life through a series of sketches and prototypes. He has a natural flair and passion for design which he is able to put to use in a wide range of applications. <br /><br />
                    <b>MANISH JETHWA <br/> CTO at Yotta</b>
                </div>
            </div>
        </div>
    </div>
</section>
<section style="background:#ededff;">
<div class="row">
   {% include elements/heading.html 
    title="Portfolio"
    text="Explore a selection of my work, showcasing engaging, functional, and impactful designs that drive real results."
%}
    {% include loops/loop-articles-limited.html %}
    <a class="button button-blue button-expanded" href="/ux-portfolio/" title="Visit my portfolio"> See More of My Work</a>
    </div>
</section>
<section>
    {% include components/index-facts.html title="Key Facts" %}
</section>
<style>
/* Custom styles here */
</style>
